function [x] = Tik(PSF, b)

% INPUT:
%     - eigA: eigenvalues of the blur operator A
%     - eigLstarL: eigenvalues of the matrix L'L
%     - b: observed image
% OUTPUT:
%     - x: Tikhonov solution computed with parameter "alpha_Tik"
%     - xhat: solution in the Fourier domain

[n,m]=size(b);

eigA=fft2(PSF);
%eigA=psf2otf(PSF,[n m]);
eigLstarL=ones(size(b));


bhat=fft2(b);
B=conj(eigA).*bhat;

alpha_Tik = gcv_sparse(eigA(:), bhat(:),eigLstarL(:));

xhat=B./((abs(eigA).^2)+alpha_Tik*eigLstarL);

x=real(ifft2(xhat));
end