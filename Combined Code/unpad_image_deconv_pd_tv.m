function I = unpad_image_deconv_pd_tv(x,pad_size)

if (length(pad_size) == 1)
    pad_size = [pad_size,pad_size];
end

[n,m]=size(x);
hfs_x1=floor((pad_size(2)-1)/2);
hfs_x2=ceil((pad_size(2)-1)/2);
hfs_y1=floor((pad_size(1)-1)/2);
hfs_y2=ceil((pad_size(1)-1)/2);

I = x(hfs_y1+1:n-hfs_y2,hfs_x1+1:m-hfs_x2);