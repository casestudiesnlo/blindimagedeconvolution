dImg=im2double(imread('images/non-blind/DSC04416.JPG'));

percnoise = 0.01;

if(ndims(dImg)==3)
    dImg=rgb2gray(dImg);
end

% set size of the matrix
[n,m]=size(dImg);

[PSFtilde,center] = psfGauss([255,255],2);

%save original picture
dImg_orig=dImg;

PSF_orig = PSFtilde;

%pixel(1,1) will be used as center
PSF=padPSF(PSFtilde,size(dImg));

% circleshift the PSA, so the picture is not moved
PSFtilde=circshift(PSF,1-center);


%GPU speedup
tic;
PSFtilde=gpuArray(PSFtilde);
dImg=gpuArray(dImg);
toc;

%-----------------------------------
% BLURR PICTURE
%-----------------------------------
tic;
% calculate the eigenvalues of the point spread function
eigA=fft2(PSFtilde,size(dImg,1),size(dImg,2));
%eigA=psf2otf(PSFtilde,[size(dImg,1) size(dImg,2)]);

b=real(ifft2(eigA.*fft2(dImg))); 
toc;
% add gaussian noise to the picture
randn('seed',27)
e=randn(size(b));
e=e/norm(e(:));
e=percnoise*e*norm(b(:));
dImg=b+e; 


