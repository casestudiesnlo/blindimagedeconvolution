function test_runner_for_gui(dImg,blindFlag, percnoise, blurredImageAxes, imageTargetList, accuracyRRETextList,accuracyPSNRTextList,accuracySSIMTextList, BestRREText,BestPSNRText,BestSSIMText, runningTimeTextList, BestTimeText, originalKernelAxes, kernelTargetList, AlgorithmIndex, psf_choice, psf_axes)
%-----------------------------------
% How to update this file
%-----------------------------------
% To add a new algorithm increase AmountOfAlgorithms by one, then add a new
% code part in IMAGE DEBLURRING like follows:

% X Algorithm version 
% Y Algorithm modification
% Z Counter for new Algorithm(increment the last one by 1)
% Params The Parameter for the new Algorithm

%j=<Z>
%tic;
%[dImg_deb(:,:,j)] = Algo_v<X>_<Y>(<Params>);
%Times(i,j)=toc;
%RRE(i,j)= norm(dImg_deb(:,:,j)-dImg)/normx;



wb = waitbar(0, 'calculating..');

%-----------------------------------
% Set Parameters
%-----------------------------------

%how many times should this script run. Will average over all times.(except
%first one)
runs = 2;
% choose picture
%picture = 'cameraman.tif';
% set degree of noise
%percnoise = 0.01;
% choose the kind of kernel of the deblurred picture (here hardcoded)
% set 1 for out of focus, 2 for Gaussian and 3 for motion
%psf_choice = 2;


% Set parameters of the point spread function, these are currently
% hardcoded
switch psf_choice    
    case 1 
        [PSFtilde, center] = psfDefocus([11,11],4);
    case 2      
        [PSFtilde,center] = psfGauss([150,150],2);
    case 3         
        load motionpsf3, center = [12,11];
        PSFtilde = ps;
    case 4
        PSFtilde= 1;
        center =1;
end

% transform the picture into a matrix
%dImg=im2double(imread(picture)); 
% save the original image
%imwrite(uint8(dImg),'cameraman.tif');

%check if image is colored or grey
if(ndims(dImg)==3)
    dImg=rgb2gray(dImg);
end

% set size of the matrix
[n,m]=size(dImg);

%save original picture
dImg_orig=dImg;

PSF_orig = PSFtilde;

%pixel(1,1) will be used as center
PSF=padPSF(PSFtilde,size(dImg));

% circleshift the PSA, so the picture is not moved
PSFtilde=circshift(PSF,1-center);

%dont blurr if blind is set
if(blindFlag==0)
    %-----------------------------------
    % BLURR PICTURE
    %-----------------------------------
    % calculate the eigenvalues of the point spread function
    eigA=fft2(PSFtilde,size(dImg,1),size(dImg,2));
    %eigA=psf2otf(PSFtilde,[size(dImg,1) size(dImg,2)]);

    b=real(ifft2(eigA.*fft2(dImg))); 
    % add gaussian noise to the picture
    randn('seed',27)
    e=randn(size(b));
    e=e/norm(e(:));
    e=percnoise*e*norm(b(:));
    dImg=b+e; 
end

%Show blurred image
axes(blurredImageAxes);
imshow(dImg);


% calculate the norm of the original picture to compare it to the 
% reconstructed one
normx=norm(dImg_orig(:));

%How many different Algorithms are tested
AmountOfAlgorithms = 8;

%
kernel_size=29;

Times = inf*ones(runs,AmountOfAlgorithms);
RRE   = inf*ones(runs,AmountOfAlgorithms);
PSNR   = zeros(runs,AmountOfAlgorithms);
SSIM   = zeros(runs,AmountOfAlgorithms);
dImg_deb = zeros(n,m,AmountOfAlgorithms);
dKernel_deb = zeros(kernel_size,kernel_size,AmountOfAlgorithms);

waitbar(0.1);

algoName{1} = 'Algo_v1_c';
algoName{2} = 'Algo_v1_h';
algoName{3} = 'Perrone';
algoName{4} = 'Tik';
algoName{5} = 'Tik2';
algoName{6} = 'deconv_pd_tv';
algoName{7} = 'Algo_v2';
algoName{8} = 'coarse_to_fine';

for i = 1:runs

%--------------------------------------
% IMAGE DEBLURRING
%--------------------------------------

%---------------------------------------
% Start the deblurring algorithms here
% clear before each one so they all have the same starting condition

if(blindFlag==0)
    %clear;
    j=1;
    tic;
    [dImg_deb(:,:,j)] = Algo_v1_c(PSF_orig, dImg,2);
    Times(i,j)=toc;
    RRE(i,j)= norm(dImg_deb(:,:,j)-dImg_orig)/normx;
    PSNR(i,j)= psnr(dImg_deb(:,:,j),dImg_orig);
    SSIM(i,j) = ssim(dImg_deb(:,:,j),dImg_orig);
    waitbar(0.1+((i-1)/runs)*0.8+(j/(AmountOfAlgorithms*runs))*0.8);

    %clear;
    j=2;
    tic;
    [dImg_deb(:,:,j)] = Algo_v1_h(PSF_orig, dImg,2);
    Times(i,j)=toc;
    RRE(i,j)= norm(dImg_deb(:,:,j)-dImg_orig)/normx;
    PSNR(i,j)= psnr(dImg_deb(:,:,j),dImg_orig);
    SSIM(i,j) = ssim(dImg_deb(:,:,j),dImg_orig);
    waitbar(0.1+((i-1)/runs)*0.8+(j/(AmountOfAlgorithms*runs))*0.8);

%     %clear;
%     j=3;
%     tic;
%     [dImg_deb(:,:,j)] = Algo_v1_d(PSF_orig, dImg,2,50);
%     Times(i,j)=toc;
%     RRE(i,j)= norm(dImg_deb(:,:,j)-dImg_orig)/normx;
%     PSNR(i,j)= psnr(dImg_deb(:,:,j),dImg_orig);
%     SSIM(i,j) = ssim(dImg_deb(:,:,j),dImg_orig);
%     waitbar(0.1+((i-1)/runs)*0.8+(j/(AmountOfAlgorithms*runs))*0.8);

    % Tikhonov
    j=4;
    tic;
    [dImg_deb(:,:,j)] = Tik(PSFtilde, dImg);
    Times(i,j)=toc;
    RRE(i,j)= norm(dImg_deb(:,:,j)-dImg_orig)/normx;
    PSNR(i,j)= psnr(dImg_deb(:,:,j),dImg_orig);
    SSIM(i,j) = ssim(dImg_deb(:,:,j),dImg_orig);
    waitbar(0.1+((i-1)/runs)*0.8+(j/(AmountOfAlgorithms*runs))*0.8);

    % Tikhonov2
    j=5;
    tic;
    [dImg_deb(:,:,j)] = Tik2(PSFtilde, dImg);
    Times(i,j)=toc;
    RRE(i,j)= norm(dImg_deb(:,:,j)-dImg_orig)/normx;
    PSNR(i,j)= psnr(dImg_deb(:,:,j),dImg_orig);
    SSIM(i,j) = ssim(dImg_deb(:,:,j),dImg_orig);
    waitbar(0.1+((i-1)/runs)*0.8+(j/(AmountOfAlgorithms*runs))*0.8);

%     TVDB Perrone and Favaro[4]
%     j=3;
%     pf_MK = 5;
%     pf_NK = 5;
%     pf_lambda = 3.5e-4;
%     pf_params.gammaCorrection = false;
%     pf_params.gamma = 0;
%     pf_params.iters = 1000;
%     pf_params.visualize = 0;
%     tic;
%     [new_img, p_f_PSF] = deblur(dImg,pf_MK,pf_NK,pf_lambda,pf_params);
%     Times(i,j)=toc;
%     %resize image as the output is not the original size
%     dImg_deb(:,:,j)=imresize(new_img,[n m]);
%     RRE(i,j)= norm(dImg_deb(:,:,j)-dImg_orig)/normx;
%     PSNR(i,j)= psnr(dImg_deb(:,:,j),dImg_orig);
%     SSIM(i,j) = ssim(dImg_deb(:,:,j),dImg_orig);


%     % TV
    options.maxit   = 10;
    options.tau     = 50/(1*sqrt(8));
    options.sigma   = (1/8)*(1/options.tau);

    j=6;
    tic;
    [dImg_deb(:,:,j)] = deconv_pd_tv(dImg,PSF_orig,1e-1,options);
    Times(i,j)=toc;
    RRE(i,j) = norm(dImg_deb(:,:,j)-dImg)/normx;
    PSNR(i,j)= psnr(dImg_deb(:,:,j),dImg_orig);
    SSIM(i,j) = ssim(dImg_deb(:,:,j),dImg_orig);
    waitbar(0.1+((i-1)/runs)*0.8+(j/(AmountOfAlgorithms*runs))*0.8);

    %Should get PSF not tilde for fairness?
    j=7;
    tic;
    [dImg_deb(:,:,j)] = Algo_v2(PSF_orig, dImg,2);
    Times(i,j)=toc;
    RRE(i,j)= norm(dImg_deb(:,:,j)-dImg)/normx;
    PSNR(i,j)= psnr(dImg_deb(:,:,j),dImg_orig);
    SSIM(i,j) = ssim(dImg_deb(:,:,j),dImg_orig);
    waitbar(0.1+((i-1)/runs)*0.8+(j/(AmountOfAlgorithms*runs))*0.8);
end

% j=8;
% tic;
% [dImg_deb(:,:,j), dKernel_deb(:,:,j)] = coarseToFine(dImg, kernel_size,2);
% Times(i,j)=toc;
% RRE(i,j)= norm(dImg_deb(:,:,j)-dImg)/normx;
% PSNR(i,j)= psnr(dImg_deb(:,:,j),dImg_orig);
% SSIM(i,j) = ssim(dImg_deb(:,:,j),dImg_orig);
% waitbar(0.1+((i-1)/runs)*0.8+(j/(AmountOfAlgorithms*runs))*0.8);

%show progress
waitbar(0.1+(i/runs)*0.8);

end

waitbar(0.9);

%--------------------------------------
% OUTPUT
%--------------------------------------

%remove first run, as this one is mostens skewed
Times(1,:)=[];
RRE(1,:)=[];
PSNR(1,:)=[];
SSIM(1,:)=[];

%Calculate running time information
avgTime = sum(Times,1)/AmountOfAlgorithms;
BestTime = min(avgTime);
BestTimeAlg = find(avgTime==BestTime);


%Calculate accuracy information

%RRE
avgRRE = sum(RRE,1)/AmountOfAlgorithms;
BestRRE = min(avgRRE);
BestRREAlg = find(avgRRE==BestRRE);

%Peak Signal-to-Noise Ratio (PSNR)
avgPSNR = sum(PSNR,1)/AmountOfAlgorithms;
BestPSNR = max(avgPSNR);
BestPSNRAlg = find(avgPSNR==BestPSNR);

%Structural Similarity Index (SSIM) 
avgSSIM = sum(SSIM,1)/AmountOfAlgorithms;
BestSSIM = max(avgSSIM);
BestSSIMAlg = find(avgSSIM==BestSSIM);

%update Images in GUI   
for i = 1:3
    indexAlgo=AlgorithmIndex{i};
    axes(imageTargetList{i});
    %imagesc(dImg_deb(:,:,indexAlgo));
    imshow(dImg_deb(:,:,indexAlgo));
    set(accuracyRRETextList{i},'String',avgRRE(indexAlgo));
    set(accuracyPSNRTextList{i},'String',avgPSNR(indexAlgo));
    set(accuracySSIMTextList{i},'String',avgSSIM(indexAlgo));
    set(runningTimeTextList{i},'String',avgTime(indexAlgo));
    axes(kernelTargetList{i});
    imagesc(dKernel_deb(:,:,indexAlgo));
    %imshow(dKernel_deb(:,:,indexAlgo)*255);
end
axes(originalKernelAxes);
imshow(PSF_orig*255);
set(BestRREText,'String',[ algoName{BestRREAlg(1)} ' has the lowest RRE with ' num2str(BestRRE(1))]);
set(BestPSNRText,'String',[ algoName{BestPSNRAlg(1)} ' has the highest PSNR with ' num2str(BestPSNR(1))]);
set(BestSSIMText,'String',[ algoName{BestSSIMAlg(1)} ' has the highest SSIM with ' num2str(BestSSIM(1))]);
set(BestTimeText,'String',[ algoName{BestTimeAlg(1)} ' has the best running time with ' num2str(BestTime(1))]);
waitbar(1);  
close(wb);


%Save information in text-file
fileID = fopen('output.txt','w');

%print info for all algorithms
for i = 1:AmountOfAlgorithms

fprintf(fileID,[ 'Algorithm ' algoName{i} '\r\n']);    
fprintf(fileID,'Average RRE: %.4f \r\n',avgRRE(i));
fprintf(fileID,'Average PSNR: %.4f \r\n',avgPSNR(i));
fprintf(fileID,'Average SSIM: %.4f \r\n',avgSSIM(i));
fprintf(fileID,'Average running time: %.4f\r\n',avgTime(i));
fprintf(fileID,'\r\n');

end

% Show best Algorithms. Ties only show the first algorithm
fprintf(fileID,[ algoName{BestRREAlg(1)} ' has the lowest RRE with ' num2str(BestRRE(1)) '\r\n']);    
fprintf(fileID,[ algoName{BestPSNRAlg(1)} ' has the highest PSNR with ' num2str(BestPSNR(1)) '\r\n']);  
fprintf(fileID,[ algoName{BestSSIMAlg(1)} ' has the highest SSIM with ' num2str(BestSSIM(1)) '\r\n']);  
fprintf(fileID,[ algoName{BestTimeAlg(1)} ' has the best running time with ' num2str(BestTime(1)) '\r\n']); 

fclose(fileID);

% save the blurred image
imwrite(uint8(dImg*255),'cameraman_blurred.tif');

% save the deblurred images from different algorithms. Only saves last
for i=1:AmountOfAlgorithms
    
filename = sprintf('cameraman_deblurred%i.tif',i) ;
imwrite(uint8(dImg_deb(:,:,i)*255),filename);

end
