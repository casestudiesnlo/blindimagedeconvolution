%% Zum testen der schwierigen Kernel (parabel,schleife,spirale)

close all

% laden der Kernel Datenbank
load Difficult_Kernel.mat

% Laden und blurren des Bildes
picture = 'cameraman.tif';
dImg=im2double(imread(picture));
dImg=dImg(:,:,1);

dImg_v_blurred = imfilter(dImg,Kernel_v,'replicate');
dImg_spirale_blurred = imfilter(dImg,Kernel_spirale,'replicate');
dImg_schleife_blurred = imfilter(dImg,Kernel_schleife,'replicate');

%-----------------------------------
% BLURR PICTURE
%-----------------------------------
% calculate the eigenvalues of the point spread function
%eigA_v=fft2(Kernel_v,size(dImg,1),size(dImg,2));
%eigA_spirale=fft2(Kernel_spirale,size(dImg,1),size(dImg,2));
%eigA_schleife=fft2(Kernel_schleife,size(dImg,1),size(dImg,2));
%eigA=psf2otf(PSFtilde,[size(dImg,1) size(dImg,2)]);

%b_v=real(ifft2(eigA_v.*fft2(dImg))); 
%b_spirale=real(ifft2(eigA_spirale.*fft2(dImg))); 
%b_schleife=real(ifft2(eigA_schleife.*fft2(dImg))); 
% % add gaussian noise to the picture
% randn('seed',27)
% e=randn(size(b));
% e=e/norm(e(:));
% e=percnoise*e*norm(b(:));
% dImg=b+e; 

%----------------------------------
% %% Variante von Johannes zum Test des Kernels
% % im predictionFilter wird das latente Bild eingelesen, dadurch perfekte
% % Bestimmung des Gradienten?! 
% sigma_s = 2;
% sigma_r = 0.5;
% dt = 1;
% r = 2;
% [Px, Py] = predictionFilter(dImg, sigma_s, sigma_r, dt, r, 45, 9); 
% 
% % calculate image derivates
% %[Px,Py] = imgradientxy(dImg,'intermediate');
% 
% % Precompute the Fouriertransformations
% imageSize = size(dImg_spirale);
% K = psfGauss(25,1);
% fft_blrd_img=fft2(dImg_spirale); 
% fft_d=psf2otf(1,imageSize);
% fft_dx=psf2otf([1,-1],imageSize);
% fft_dy=psf2otf([1;-1],imageSize);
% fft_dxx=psf2otf([1,-2,1],imageSize);
% fft_dyy=psf2otf([1;-2;1],imageSize);
% fft_dxy=psf2otf([1,-1;-1,1],imageSize);
% 
% tic
% kernel_1=kernel_est_direct(Px, Py, K, fft_blrd_img, fft_dx, fft_dy, fft_dxx, fft_dyy, fft_dxy);
% toc
% 
% tic
% kernel_2=kernel_est(Px, Py, K, fft_blrd_img, fft_dx, fft_dy, fft_dxx, fft_dyy, fft_dxy);
% toc
% 
%----------------------------------


%----------------------------------
%% 
[P_v, kernel_v]=coarseToFine_direct_center(dImg_v,25);
[P_spirale, kernel_spirale]=coarseToFine_direct_center(dImg_spirale,25);
[P_schleife, kernel_schleife]=coarseToFine_direct_center(dImg_schleife,25);

dImg_v_deblurred=Algo_v2(Kernel_v,dImg_v_blurred,2);
dImg_spirale_deblurred=Algo_v2(Kernel_spirale,dImg_spirale_blurred,2);
dImg_schleife_deblurred=Algo_v2(Kernel_schleife,dImg_schleife_blurred,2);

figure, subplot(3,3,1), subimage(P_v), 
subplot(3,3,2), subimage(P_spirale), 
subplot(3,3,3), subimage(P_schleife),
subplot(3,3,4), subimage(100*Kernel_v),
subplot(3,3,5), subimage(100*Kernel_spirale),
subplot(3,3,6), subimage(100*Kernel_schleife),
subplot(3,3,7), subimage(100*kernel_v),
subplot(3,3,8), subimage(100*kernel_spirale),
subplot(3,3,9), subimage(100*kernel_schleife),

