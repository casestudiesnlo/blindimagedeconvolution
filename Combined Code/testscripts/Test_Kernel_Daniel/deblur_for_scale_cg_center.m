function [ L, K ] = deblur_for_scale( B, L, K )
%DEBLUR_FOR_SCALE blind deblur for an image
% B   blurred image  
% L   initial latent image
% K   initial kernel (used with CG)

MAX_ITER = 30;  % 7 in the paper
kernelSize = size(K, 1);
assert(mod(kernelSize,2) == 1, 'kernel size has to be an uneven number')

sigma_s = 2; % An increase or decrease in sigma_s doesn't seem to change anything
sigma_r = 0.5; % A change in sigma_s doesn't change the kernel
dt = 1; % If we set dt too high, we the kernel changes
r = 2; % An increase in r doesn't seem to change anything
gamma = 2;

% Precompute the Fouriertransformations
imageSize = size(B);
fft_blrd_img=fft2(B); 
fft_d=psf2otf(1,imageSize);
fft_dx=psf2otf([1,-1],imageSize);
fft_dy=psf2otf([1;-1],imageSize);
fft_dxx=psf2otf([1,-2,1],imageSize);
fft_dyy=psf2otf([1;-2;1],imageSize);
fft_dxy=psf2otf([1,-1;-1,1],imageSize);

% Precompute delta
w0=50; w1=25; w2=12.5; % weights for summing up depending on the derivatives 
delta0=w0*conj(fft_d).*fft_d;
delta1=w1*conj(fft_dx).*fft_dx+w1*conj(fft_dy).*fft_dy;
delta2=w2*conj(fft_dxx).*fft_dxx+w2*conj(fft_dyy).*fft_dyy+w2*conj(fft_dxy).*fft_dxy;
delta=delta0+delta1+delta2;

figure
for k = 1:MAX_ITER
    % Apply Filter to extract the edges of the image
    [Px, Py] = predictionFilter(L, sigma_s, sigma_r, dt, r, k, kernelSize); 

    % Estimate Kernel
    K = kernel_est(Px, Py, K, fft_blrd_img, fft_dx, fft_dy, fft_dxx, fft_dyy, fft_dxy);
    %K = kernel_est_direct(Px, Py, K, fft_blrd_img, fft_dx, fft_dy, fft_dxx, fft_dyy, fft_dxy);
    K = center_kernel(K);
    imagesc(K); drawnow;
    
    L = simpleDeconvolution(K, gamma, delta, fft_blrd_img, fft_dx, fft_dy); % use code of paper
    %P = Tik(K, B); % for testing: use Tikhonov regularization
    
end


end

