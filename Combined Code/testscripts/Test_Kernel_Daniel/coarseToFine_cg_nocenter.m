function [P, kernel] = coarseToFine(B, kernelSize, scaleMultiplier)
% coarseToFine interatively estimate kernel, starting from small image
% scales kernel until it has the value of 'smallestKernelSize' (default: 3)
% ensures that kernelSize is always an integer odd number
% INPUT
% - B   blurred image
% - kernelSize
% - scaleMultiplier factor that determines how fast the size of kernel
% should decrease

    [m, n] = size(B);
    
    smallestScale = 3; % smalles kernel size   
    
    if (~exist('scaleMultiplier','var'))
        scaleMultiplier = 1.1;
    end
    
    scales = 1;  
    
    Bp{1} = B;
    Mp{1} = m;
    Np{1} = n;
    Ks{1} = kernelSize;  
    % ---------------------------------------------------------------------
    % COARSE TO FINE PART
    % build pyramid of downsampled images, starting from largest image
    while Ks{scales} > smallestScale
        scales = scales + 1;
                
        Ks{scales} = round(Ks{scales - 1} / scaleMultiplier);        
        
        % Makes kernel dimension odd
        if mod(Ks{scales},2) == 0
            Ks{scales} = Ks{scales} - 1;
        end
        
        if Ks{scales} == Ks{scales-1}
            Ks{scales} = Ks{scales} - 2;
        end
      
        if Ks{scales} < smallestScale
            Ks{scales} = smallestScale;
        end
        
        % scale image according to kernel scaling
        factor = Ks{scales - 1}/Ks{scales};
        
        Mp{scales} = round(Mp{scales - 1} / factor);
        Np{scales} = round(Np{scales - 1} / factor);        
        
        % odd image dimensions
%         if mod(Mp{scales},2) == 0
%             Mp{scales} = Mp{scales} - 1;
%         end
%         if mod(Np{scales},2) == 0
%             Np{scales} = Np{scales} - 1;
%         end
        
        Bp{scales} = imresize(B,[Mp{scales} Np{scales}], 'Method', 'bilinear');       
    end
    
    % now, start with smallest image    
    upsampledResult = Bp{scales};
    kernel = psfGauss(3,1);
    % ---------------------------------------------------------------------
    % LOOP FOR BLIND ALGORITHM
    figure
    for scale=fliplr(1:scales)
       [P, kernel] = deblur_for_scale_cg_nocenter(Bp{scale}, upsampledResult, kernel);
       
       %DEBUG
       %figure, imagesc(kernel);
       imagesc(kernel)
       %imshow(P);
       drawnow
       
       if scale > 1  % then upsample result to use for prediction in next step
         upsampledResult = imresize(P, [Mp{scale-1} Np{scale-1}], 'Method', 'bilinear');
         
         kernel = imresize(kernel, [Ks{scale-1} Ks{scale-1}], 'Method', 'bilinear');
         kernel=max(kernel-(1/20)*max(kernel(:)),0);
         kernel=kernel./(sum(kernel(:)));
       end
    end
end