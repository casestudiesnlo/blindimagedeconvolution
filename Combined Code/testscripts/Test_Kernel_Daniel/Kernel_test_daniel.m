%% Zum testen verschiedener Kernel (center,nocenter) (direct,cg)

close all

% laden der Kernel Datenbank
% load Kernel_estimation_vertikal.mat
% load Kernel_estimation_horizontal_Werbung.mat

% Laden und blurren des Bildes
picture = 'Image_2.png';
dImg=im2double(imread(picture));    

[~, kernel_direct_center_1]=coarseToFine_direct_center(dImg,29);
[~, kernel_direct_center_2]=coarseToFine_direct_center(dImg,29,2);
[~, kernel_cg_center_1]=coarseToFine_cg_center(dImg,29);
[~, kernel_cg_center_2]=coarseToFine_cg_center(dImg,29,2);
[~, kernel_cg_nocenter_1]=coarseToFine_cg_nocenter(dImg,29);
[~, kernel_cg_nocenter_2]=coarseToFine_cg_nocenter(dImg,29,2);
[~, kernel_direct_nocenter_1]=coarseToFine_direct_nocenter(dImg,29);
[~, kernel_direct_nocenter_2]=coarseToFine_direct_nocenter(dImg,29,2);


% Berechnung der geschärften Bilder
dImg_cg_center_1=Algo_v2(kernel_cg_center_1,dImg,2);
dImg_cg_center_2=Algo_v2(kernel_cg_center_2,dImg,2);
dImg_cg_nocenter_1=Algo_v2(kernel_cg_nocenter_1,dImg,2);
dImg_cg_nocenter_2=Algo_v2(kernel_cg_nocenter_2,dImg,2);
dImg_direct_center_1=Algo_v2(kernel_direct_center_1,dImg,2);
dImg_direct_center_2=Algo_v2(kernel_direct_center_2,dImg,2);
dImg_direct_nocenter_1=Algo_v2(kernel_direct_nocenter_1,dImg,2);
dImg_direct_nocenter_2=Algo_v2(kernel_direct_nocenter_2,dImg,2);

% Plot der geschärften Bilder
figure, subplot(2,4,1), subimage(dImg_cg_center_1), 
subplot(2,4,2), subimage(dImg_cg_center_2), 
subplot(2,4,3), subimage(dImg_cg_nocenter_1), 
subplot(2,4,4), subimage(dImg_cg_nocenter_2), 
subplot(2,4,5), subimage(dImg_direct_center_1),
subplot(2,4,6), subimage(dImg_direct_center_2), 
subplot(2,4,7), subimage(dImg_direct_nocenter_1), 
subplot(2,4,8), subimage(dImg_direct_nocenter_2)

% Plot der verwendeten Kernel
% figure, subplot(2,4,1), subimage(100*kernel_cg_center_1), 
% subplot(2,4,2), subimage(100*kernel_cg_center_2), 
% subplot(2,4,3), subimage(100*kernel_cg_nocenter_1), 
% subplot(2,4,4), subimage(100*kernel_cg_nocenter_2), 
% subplot(2,4,5), subimage(100*kernel_direct_center_1), 
% subplot(2,4,6), subimage(100*kernel_direct_center_2), 
% subplot(2,4,7), subimage(100*kernel_direct_nocenter_1), 
% subplot(2,4,8), subimage(100*kernel_direct_nocenter_2)