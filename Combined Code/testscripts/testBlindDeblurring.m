%% calculates the kernel and visualize it

close all
picture = 'cameraman.tif';

% set degree of noise. do we need noise?
percnoise = 0.001;

%PSFtilde = fspecial('gaussian', 5, 3);
PSFtilde = fspecial('motion', 12, 45);

dImg=im2double(imread(picture)); 

b = imfilter(dImg, PSFtilde, 'replicate');
figure, imshow(b);
% add gaussian noise to the picture
randn('seed',27);
e=randn(size(b));
e=e/norm(e(:));
e=percnoise*e*norm(b(:));
dImg_blurred=b+e; 

tic
[~, kernel] = coarseToFine(dImg_blurred, 11);
toc

tic
P = Algo_v2(kernel, dImg_blurred, 2);
toc

figure, subplot(1,2,1), subimage(dImg_blurred)
subplot(1,2,2), subimage(P)

figure, imagesc([kernel, PSFtilde]);


