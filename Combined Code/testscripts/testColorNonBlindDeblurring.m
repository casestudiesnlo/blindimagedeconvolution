%% calculates the kernel and visualize it

close all
picture = 'magic-cube-378543.jpg';

% set degree of noise.
percnoise = 0.01;

%PSFtilde = fspecial('gaussian', 5, 3);
PSFtilde = fspecial('motion', 30, 45);

dImg=im2double(imread(picture)); 
dimg_org=dImg;
dImg_Deb=dimg_org;

    b = imfilter(dImg, PSFtilde, 'replicate');
    figure, imshow(b);
    % add gaussian noise to the picture
    randn('seed',27);
    e=randn(size(b));
    e=e/norm(e(:));
    e=percnoise*e*norm(b(:));
    dImg_blurred_org=b+e; 
    
for i=1:3
    dImg=dimg_org(:,:,i);
    dImg_blurred=dImg_blurred_org(:,:,i);

    tic
    dImg_Deb(:,:,i) = Algo_v2(PSFtilde, dImg_blurred,2);
    toc
end


figure, subplot(1,2,1), subimage(dImg_blurred)
subplot(1,2,2), subimage(dImg_Deb)



