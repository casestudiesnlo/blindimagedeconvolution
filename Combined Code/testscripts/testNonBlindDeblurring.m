%% calculates the kernel and visualize it

close all
picture = 'brief.png';

% set degree of noise.
percnoise = 0.01;

%PSFtilde = fspecial('gaussian', 5, 3);
PSFtilde = fspecial('motion', 30, 45);

dImg=im2double(imread(picture)); 
% dImg=rgb2gray(dImg);

b = imfilter(dImg, PSFtilde, 'replicate');
figure, imshow(b);
% add gaussian noise to the picture
randn('seed',27);
e=randn(size(b));
e=e/norm(e(:));
e=percnoise*e*norm(b(:));
dImg_blurred=b+e; 

tic
P = Algo_v2(PSFtilde, dImg_blurred,2);
toc

RRE= norm(P-dImg)/norm(dImg(:))
PSNR= psnr(P,dImg)
SSIM = ssim(P,dImg)


figure, subplot(1,2,1), subimage(dImg_blurred)
subplot(1,2,2), subimage(P)



