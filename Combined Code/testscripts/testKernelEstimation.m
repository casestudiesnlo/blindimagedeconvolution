%% calculates the kernel and visualize it

close all
picture = 'cameraman.tif';

% set degree of noise. do we need noise?
percnoise = 0.001;

%PSFtilde = fspecial('gaussian', 5, 3);
PSFtilde = fspecial('motion', 12, 45);

dImg=im2double(imread(picture)); 

b = imfilter(dImg, PSFtilde);

% add gaussian noise to the picture
randn('seed',27);
e=randn(size(b));
e=e/norm(e(:));
e=percnoise*e*norm(b(:));
dImg_blurred=b+e; 

sigma_s = 2;
sigma_r = 0.5;
dt = 1;
r = 2;
[Px, Py] = predictionFilter(dImg, sigma_s, sigma_r, dt, r, 45, 9); 

% calculate image derivates
%[Px,Py] = imgradientxy(dImg,'intermediate');

% Precompute the Fouriertransformations
imageSize = size(dImg_blurred);
K = psfGauss(9,1);
fft_blrd_img=fft2(dImg_blurred); 
fft_d=psf2otf(1,imageSize);
fft_dx=psf2otf([1,-1],imageSize);
fft_dy=psf2otf([1;-1],imageSize);
fft_dxx=psf2otf([1,-2,1],imageSize);
fft_dyy=psf2otf([1;-2;1],imageSize);
fft_dxy=psf2otf([1,-1;-1,1],imageSize);

tic
kernel_1=kernel_est_direct(Px, Py, K, fft_blrd_img, fft_dx, fft_dy, fft_dxx, fft_dyy, fft_dxy);
toc

tic
kernel_2=kernel_est(Px, Py, K, fft_blrd_img, fft_dx, fft_dy, fft_dxx, fft_dyy, fft_dxy);
toc

%norm(kernel - PSFtilde)

subplot(3,2,1), subimage(Px)
subplot(3,2,2), subimage(Py)
subplot(3,2,3), subimage(dImg)
%subplot(3,2,4), subimage(dImg_filtered)
subplot(3,2,5), subimage(kernel_1./max(kernel_1(:)))
subplot(3,2,6), subimage(kernel_2./max(kernel_2(:)))

figure, imagesc([PSFtilde kernel_2])



