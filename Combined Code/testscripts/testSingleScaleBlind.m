%% calculates the kernel and visualize it

close all
picture = 'cameraman.tif';

% set degree of noise. do we need noise?
percnoise = 0;

%PSFtilde = fspecial('gaussian', 5, 3);
PSFtilde = fspecial('motion', 12, 45);

dImg=im2double(imread(picture)); 

b = imfilter(dImg, PSFtilde);

% add gaussian noise to the picture
randn('seed',27);
e=randn(size(b));
e=e/norm(e(:));
e=percnoise*e*norm(b(:));
dImg_blurred=b+e; 


tic
[P, kernel] = deblur_for_scale(dImg_blurred, dImg_blurred, psfGauss(size(PSFtilde,1)));
toc


subplot(1,2,1), subimage(dImg_blurred)
subplot(1,2,2), subimage(P)

figure, imagesc([kernel, PSFtilde]);


