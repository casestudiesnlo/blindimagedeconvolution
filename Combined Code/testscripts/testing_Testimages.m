%% calculates the deblurred Image of our testing images with and without the true kernel

close all

% select a Image out of the set of testimages, i doesn't have to be
% converted
picture = 'Image_2.png';

% the true Kernel has parameter between 25-28 and 88-90 . I can't extract it
% exactly from the Kernelpictures
Kernel_true = fspecial('motion', 27, 89);

dImg=im2double(imread(picture)); 

%
tic
[P, kernel] = coarseToFine(dImg, 29,2);
toc

tic
dImg_deblurred = Algo_v2(Kernel_true,dImg,2);
toc

tic
P = Algo_v2(kernel, dImg, 2);
toc

%M1 = M(dImg, Kernel_true);

figure, subplot(1,3,1), subimage(dImg)
subplot(1,3,2), subimage(dImg_deblurred)
subplot(1,3,3), subimage(P)



