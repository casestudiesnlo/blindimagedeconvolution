%% calculates the kernel and visualize it

close all
%picture = 'Image_2.png';
picture = 'cameraman.tif';

load Difficult_Kernel.mat

psf = imgaussfilt(Kernel_spirale,0.7);

dImg=im2double(imread(picture)); 

dImg_blurred = imfilter(dImg, psf,'replicate');


tic
[P,kernel_1] = coarseToFine(dImg_blurred, 17, 1.5);
toc 

tic
P_1 = Algo_v2(kernel_1, dImg_blurred, 2);
%P_real = Algo_v2(psf, dImg_blurred, 2);
toc

figure, subplot(1,3,1), subimage(dImg)
subplot(1,3,2), subimage(dImg_blurred)
subplot(1,3,3), subimage(P_1)
%subplot(1,5,4), subimage(P_2)
%subplot(1,5,5), subimage(P_real)

psf_ = zeros(17,17); psf_(1:13,1:13) = psf;
figure, imagesc([psf_ kernel_1])



