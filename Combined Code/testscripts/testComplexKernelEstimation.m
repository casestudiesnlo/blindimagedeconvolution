%% calculates the kernel and visualize it

close all
picture = 'Image_2.png';
%picture = 'cameraman.tif';

%PSFtilde = fspecial('gaussian', 5, 3);
psf1 = fspecial('motion', 15, 45);
psf2 = fspecial('motion', 17, 315);

psf = zeros(25,25);
psf(13:23,2:12) = psf1;
psf(13:25,13:25) = psf2;

psf=max(psf-(1/20)*max(psf(:)),0);
         psf=psf./(sum(psf(:)));
psf = center_kernel(psf);

s = 51;
psf = sinusKernel(s)';

dImg=im2double(imread(picture)); 

dImg_blurred = imfilter(dImg, psf,'replicate');


tic
[P,kernel_1] = coarseToFine(dImg_blurred, 57, 1.5);
toc 

tic
P_1 = Algo_v2(kernel_1, dImg_blurred, 2);
%P_real = Algo_v2(psf, dImg_blurred, 2);
toc

figure, subplot(1,3,1), subimage(dImg)
subplot(1,3,2), subimage(dImg_blurred)
subplot(1,3,3), subimage(P_1)
%subplot(1,5,4), subimage(P_2)
%subplot(1,5,5), subimage(P_real)

psf_ = zeros(57,57); psf_(1:s,1:s) = psf;
figure, imagesc([psf_ kernel_1])



