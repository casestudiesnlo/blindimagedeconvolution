function x = deconv_pd_tv(img,filt,mu,options)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% INNER FUNCTIONS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    %function [D,Dt] = buildDDt(mn,tmn,m,n)
    %    % build D and Dt
    %    ones_mn     = ones(mn,1);
    %    ones_m      = ones(m,1);
    %
    %    Dx          = spdiags([ones_mn,-ones_mn,ones_mn],[-m*(n-1),0,m],mn,mn);
    %
    %    Dy_Base     = spdiags([ones_m,-ones_m,ones_m],[-m+1,0,1],m,m);
    %    Dy          = kron(speye(n),Dy_Base);
    %
    %    ind         = (mod(1:tmn,2) == 0);
    %
    %    D           = sparse(tmn,mn);
    %    D(ind,:)    = Dy;
    %    D(~ind,:)   = Dx;
    %    %D           = [Dx;Dy];
    %    Dt          = D';
    %end

    function z  = K_TV(w)
        wx      = [diff(w,1,2),w(:,1)-w(:,end)]; 
        wy      = [diff(w,1,1);w(1,:)-w(end,:)];
        z       = [wx(:),wy(:)];
    end

    function z  = Kt_TV(w,mm,nn)
        wx      = reshape(w(:,1),mm,nn);
        wy      = reshape(w(:,2),mm,nn);
    
        z       = -[wx(:,1)-wx(:,end),diff(wx,1,2)] - [wy(1,:)-wy(end,:);diff(wy,1,1)];
        %z       = - z;
    end

    function z  = prox_wtv(w,alp)
        wnorm   = alp./max(sqrt(sum(w.^2,2)),alp);
        z       = repmat(wnorm,1,2).*w;
        %nr_w    = sqrt(gmat*(w.^2));
        %fac     = alp./max(nr_w,alp);
        %z       = fac(g).*w;
    end

    function z  = prox_quad(w,fb,fk)
        z       = fft2(fb + w)./fk;
        z       = real(ifft2(z));
        %z       = z(:);
    end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% INITIALIZATION & PARAMETERS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

pad_sz          = 50/2;
img             = pad_image(img,2*pad_sz); 

[m,n]           = size(img);
filt            = rot90(filt,2);

mn              = m*n;
%tmn             = 2*mn;

%[D,Dt]          = buildDDt(mn,tmn,m,n);

tau             = options.tau;
sigma           = options.sigma;
%gamma           = 1e-2;

K               = @(w) K_TV(w);
Kt              = @(w) Kt_TV(w,m,n);

F               = fft2(filt,m,n);
fft_img         = real(ifft2(conj(F).*fft2(img)));
fft_div         = conj(F).*F + 1/tau;

%groups          = (1:tmn)';
%groups          = floor((groups+1)/2);
%gmat            = sparse(groups,1:tmn,ones(tmn,1),mn,tmn,tmn);

proxSigFStar    = @(w) prox_wtv(w,mu);
proxTauG        = @(w) prox_quad(w/tau,fft_img,fft_div);

y               = zeros(mn,2);
x               = img; 

x_bar           = x;

%obj             = 0;

fprintf(1,'\n');

for iter = 1:options.maxit
    y           = proxSigFStar(y + sigma*K(x_bar));

    x_old       = x;
    x           = proxTauG(x - tau*Kt(y));

    %theta       = 1/(sqrt(1+2*gamma*sigma));
    %sigma       = theta*sigma;
    %tau         = tau/theta;
    theta       = 1;
    x_bar       = x + theta*(x - x_old);
    
    %obj0        = obj;
    %obj         = 0.5*norm(x-b)^2 + mu*sum(sqrt(gmat*(D*x).^2));
    
    %if mod(iter,10) == 0
    %    fprintf(1,'Iter: %4i - Relative Error: %1.3e\n',iter,norm(x-x_old,'fro')/norm(x_old,'fro'));
    %end
end 

hs1     = floor((size(filt,1)-1)/2);
hs2     = floor((size(filt,2)-1)/2);

%x       = reshape(x,m,n);
x       = x([end-hs1+1:end,1:end-hs1],[end-hs2+1:end,1:end-hs2],:);

%% "unpad" image 
x       = unpad_image_deconv_pd_tv(x,2*pad_sz);

end