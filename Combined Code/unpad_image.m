function I = unpad_image(x,pad_size)

if (length(pad_size) == 1)
    pad_size = [pad_size,pad_size];
end
[n,m] =size(x);
I = x(pad_size(1)+1:n-pad_size(1),pad_size(2)+1:m-pad_size(2));