function varargout = gui_test_runner(varargin)
% GUI_TEST_RUNNER MATLAB code for gui_test_runner.fig
%      GUI_TEST_RUNNER, by itself, creates a new GUI_TEST_RUNNER or raises the existing
%      singleton*.
%
%      H = GUI_TEST_RUNNER returns the handle to a new GUI_TEST_RUNNER or the handle to
%      the existing singleton*.
%
%      GUI_TEST_RUNNER('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in GUI_TEST_RUNNER.M with the given input arguments.
%
%      GUI_TEST_RUNNER('Property','Value',...) creates a new GUI_TEST_RUNNER or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before gui_test_runner_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to gui_test_runner_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help gui_test_runner

% Last Modified by GUIDE v2.5 20-Jun-2016 20:50:04

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @gui_test_runner_OpeningFcn, ...
                   'gui_OutputFcn',  @gui_test_runner_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before gui_test_runner is made visible.
function gui_test_runner_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to gui_test_runner (see VARARGIN)


%Initialize view

%load placeholder
handles.placeholder = cell(2,1);
handles.placeholder = im2double(imread('images/placeholder.png'));
imshow(imread('images/placeholder.png'));


handles = showPlaceholder(handles);

handles.images = cell(2,1);

handles = initializeNonBlind(handles);

% Choose default command line output for gui_test_runner
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes gui_test_runner wait for user response (see UIRESUME)
% uiwait(handles.figure1);


function handles = showPlaceholder(handles)
% Show placeholder as nothing was deblurred
axes(handles.axes2);
imshow(handles.placeholder);
axes(handles.axes3);
imshow(handles.placeholder);
axes(handles.axes4);
imshow(handles.placeholder);
axes(handles.axes5);
imshow(handles.placeholder);
axes(handles.axes6);
imshow(handles.placeholder);
axes(handles.axes7);
imshow(handles.placeholder);
axes(handles.axes8);
imshow(handles.placeholder);

function handles = initializeNonBlind(handles)
%set blind to true
handles.blind=0;

%Set initial Algorithm names
algoName{1} = 'Algo_v1_c';
algoName{2} = 'Algo_v1_h';
algoName{3} = 'Perrone';
algoName{4} = 'Tik';
algoName{5} = 'Tik2';
algoName{6} = 'deconv_pd_tv';
algoName{7} = 'Algo_v2';
algoName{8} = 'coarse_to_fine';

set(handles.popupmenu6,'String',algoName);
set(handles.popupmenu7,'String',algoName);
set(handles.popupmenu8,'String',algoName);


%Set standard Algorithms
handles.AlgorithmIndex{1} = 1;
handles.AlgorithmIndex{2} = 1;
handles.AlgorithmIndex{3} = 1;

%set Standard Kernel
kernelText{1} = 'Defocus';
kernelText{2} = 'Gaussian';
kernelText{3} = 'Motion';
kernelText{4} = 'none';

set(handles.popupmenu2,'String',kernelText);
handles.kernelIndex=1;

%Set initial Images
picture{1} = 'Cameraman';
picture{2} = 'Chess';
picture{3} = 'Fox';
picture{4} = 'Sign';
picture{5} = 'City';
picture{6} = 'CFG';
picture{7} = 'Rubiks cube';
picture{8} = 'Tux';
picture{9} = 'Letter';

set(handles.popupmenu1,'String',picture);

% load images
handles.images{1} = im2double(imread('images/non-blind/cameraman.tif'));
handles.images{2} = im2double(imread('images/non-blind/chess-1215079.jpg'));
handles.images{3} = im2double(imread('images/non-blind/DSC04416.JPG'));
handles.images{4} = im2double(imread('images/non-blind/DSC05735.JPG'));
handles.images{5} = im2double(imread('images/non-blind/DSC06786.JPG'));
handles.images{6} = im2double(imread('images/non-blind/gauss.png'));
handles.images{7} = im2double(imread('images/non-blind/magic-cube-378543.jpg'));
handles.images{8} = im2double(imread('images/non-blind/tux.jpg'));
handles.images{9} = im2double(imread('images/blind/brief.png'));
handles.images{10} = im2double(imread('images/blind/voltaren.png'));
handles.images{11} = im2double(imread('images/blind/gaussian_with_30_5qrcode.jpeg'));

handles = setImage(handles, 1);

function handles = initializeBlind(handles)
%set blind to false
handles.blind=1;

% %Set initial Algorithm names
% algoName{1} = 'dfs';
% 
% set(handles.popupmenu6,'String',algoName);
% set(handles.popupmenu7,'String',algoName);
% set(handles.popupmenu8,'String',algoName);
% 
% %Set standard Algorithms
% handles.AlgorithmIndex{1} = 1;
% handles.AlgorithmIndex{2} = 1;
% handles.AlgorithmIndex{3} = 1;
% 
% %Set initial Images
% picture{1} = 'Photographer';
% picture{2} = 'Winter';
% picture{3} = 'Rubiks Cube';
% picture{4} = 'QR-code';
% picture{5} = 'Bridge';
% 
% set(handles.popupmenu1,'String',picture);
% 
% % load images
% handles.images{1} = im2double(imread('images/blind/cameraman.tif'));
% handles.images{2} = im2double(imread('images/blind/motion_with_40_270winter.JPG'));
% handles.images{3} = im2double(imread('images/blind/motion_with_80_0magic-cube.jpg'));
% handles.images{4} = im2double(imread('images/blind/gaussian_with_30_5qrcode.jpeg'));
% handles.images{5} = im2double(imread('images/blind/motion_with_60_0landscape.JPG'));
% 
% handles = setImage(handles, 1);
% 
% %set Standard Kernel
% kernelText{1} = 'blind';
% 
% set(handles.popupmenu2,'String',kernelText);
% handles.kernelIndex=1;

function handles = setImage(handles, index)
% sets current Image to image specified by index
% updates handles.img property accordingly
% shows image on the left
handles.imgIndex = index;
handles.img = handles.images{handles.imgIndex};
axes(handles.axes1);
imshow(handles.img);
set(handles.text2,'String','Original Image');


function handles = setKernel(handles, index)
% sets current Kernel to Kernel specified by index
% updates handles.kernel property accordingly
handles.kernelIndex = index;
%handles.kernel = handles.kernels{handles.kernelIndex};
%axes(handles.axes5);
%imshow(handles.kernel);

function handles = setAlgorithm(handles, index, AlgorithmNr)
% sets current Image to image specified by index
% updates handles.img property accordingly
% shows image on the left, sets right axis as active
handles.AlgorithmIndex{index} = AlgorithmNr;


% --- Outputs from this function are returned to the command line.
function varargout = gui_test_runner_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in pushbutton1.
function pushbutton1_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)




% --- Executes on button press in pushbutton2.
function pushbutton2_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in pushbutton3.
function pushbutton3_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on selection change in popupmenu1.
function popupmenu1_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns popupmenu1 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu1
% Determine the selected data set.
val = get(hObject,'Value');
% Set current data to the selected data set.
handles = setImage(handles, val);
% Save the handles structure.
guidata(hObject,handles)

% --- Executes during object creation, after setting all properties.
function popupmenu1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pushbutton4.
function pushbutton4_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if(handles.blind==0)
    handles = initializeBlind(handles);
    set(handles.pushbutton4,'String','Switch to Non-Blind Deblurring');
else
    handles = initializeNonBlind(handles);
    set(handles.pushbutton4,'String','Switch to Blind Deblurring');  
end

% Save the handles structure.
guidata(hObject,handles)



% --- Executes on button press in pushbutton4.
function pushbutton5_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

blurredImageAxes = handles.axes1;

imageTargetList{1} = handles.axes2;
imageTargetList{2} = handles.axes3;
imageTargetList{3} = handles.axes4;

accuracyRRETextList{1} = handles.text13;
accuracyRRETextList{2} = handles.text14;
accuracyRRETextList{3} = handles.text15;

accuracyPSNRTextList{1} = handles.text29;
accuracyPSNRTextList{2} = handles.text31;
accuracyPSNRTextList{3} = handles.text33;

accuracySSIMTextList{1} = handles.text35;
accuracySSIMTextList{2} = handles.text37;
accuracySSIMTextList{3} = handles.text39;

runningTimeTextList{1} = handles.text22;
runningTimeTextList{2} = handles.text24;
runningTimeTextList{3} = handles.text26;

originalKernelAxes = handles.axes5;

kernelTargetList{1} = handles.axes6;
kernelTargetList{2} = handles.axes7;
kernelTargetList{3} = handles.axes8;

BestRREText = handles.text9;
BestPSNRText = handles.text28;
BestSSIMText = handles.text41;
BestTimeText = handles.text20;

percnoise = str2double(get(handles.edit1,'String'));

set(handles.text2,'String','Blurred Image');

if(handles.blind==0)
    test_runner_for_gui( handles.img,handles.blind, percnoise, blurredImageAxes, imageTargetList, accuracyRRETextList,accuracyPSNRTextList,accuracySSIMTextList, BestRREText,BestPSNRText,BestSSIMText, runningTimeTextList, BestTimeText, originalKernelAxes, kernelTargetList, handles.AlgorithmIndex, handles.kernelIndex);
else
    [P, kernel] = deblur_for_scale(handles.img, 5);
    axes(imageTargetList{1});
    imshow(P);
    axes(kernelTargetList{1});
    imshow(kernel);
end

% --- Executes on selection change in popupmenu2.
function popupmenu2_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns popupmenu2 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu2
% Determine the selected data set.
val = get(hObject,'Value');
% Set current data to the selected data set.
handles = setKernel(handles, val);
% Save the handles structure.
guidata(hObject,handles)



% --- Executes during object creation, after setting all properties.
function popupmenu2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in popupmenu6.
function popupmenu6_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns popupmenu6 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu6
% Determine the selected data set.
val = get(hObject,'Value');
% Set current data to the selected data set.
handles = setAlgorithm(handles, 1, val);
% Save the handles structure.
guidata(hObject,handles)

% --- Executes during object creation, after setting all properties.
function popupmenu6_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in popupmenu7.
function popupmenu7_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu7 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns popupmenu7 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu7
% Determine the selected data set.
val = get(hObject,'Value');
% Set current data to the selected data set.
handles = setAlgorithm(handles, 2, val);
% Save the handles structure.
guidata(hObject,handles)

% --- Executes during object creation, after setting all properties.
function popupmenu7_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu7 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in popupmenu8.
function popupmenu8_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu8 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns popupmenu8 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu8
% Determine the selected data set.
val = get(hObject,'Value');
% Set current data to the selected data set.
handles = setAlgorithm(handles, 3, val);
% Save the handles structure.
guidata(hObject,handles)

% --- Executes during object creation, after setting all properties.
function popupmenu8_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu8 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit1_Callback(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit1 as text
%        str2double(get(hObject,'String')) returns contents of edit1 as a double


% --- Executes during object creation, after setting all properties.
function edit1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
