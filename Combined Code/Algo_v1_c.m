function [img_deblurred] = Algo_v1_c(kernel, blurred_img, gamma)

% INPUT:
%    - kernel: Point spread function
%    - blurred_img: blurred image 
%    - gamma: gamma value from paper
% OUTPUT:
%    - img_deblurred: deblurred image

[n,m]=size(blurred_img);

% Calculate derivatives of kernel
d_x=[-1,1];
d_y=[-1;1];
d_xx=[-1,2,-1];
d_yy=[-1;2;-1];
d_xy=[1,-1;-1,1];

% paper parameters (not used atm)
psi_x=0;
psi_y=0;

% weights for summing up depending on the derivatives 
w0=50;
w1=25;
w2=12.5;

% Fourier transform everything here to save time later; needs to have right
% dimension
fft_blrd_img=fft2(blurred_img,n, m);
fft_kernel=psf2otf(kernel,[n m]);
fft_psi_x=psf2otf(psi_x,[n m]);
fft_psi_y=psf2otf(psi_y,[n m]);
fft_d=psf2otf(1,[n m]);
fft_dx=psf2otf(d_x,[n m]);
fft_dy=psf2otf(d_y,[n m]);
fft_dxx=psf2otf(d_xx,[n m]);
fft_dyy=psf2otf(d_yy,[n m]);
fft_dxy=psf2otf(d_xy,[n m]);

%calculate delta
delta0=w0*conj(fft_d).*fft_d;
delta1=w1*conj(fft_dx).*fft_dx+w1*conj(fft_dy).*fft_dy;
delta2=w2*conj(fft_dxx).*fft_dxx+w2*conj(fft_dyy).*fft_dyy+w2*conj(fft_dxy).*fft_dxy;
delta=delta0+delta1+delta2;

%calculate counter parts
c1=conj(fft_kernel).*fft_blrd_img.*delta;
c2=gamma*conj(fft_dx).*fft_psi_x;
c3=gamma*conj(fft_dy).*fft_psi_y;
counter=c1+c2+c3;

%calculate denominator parts
d1=conj(fft_kernel).*fft_kernel.*delta;
d2=gamma*conj(fft_dx).*fft_dx;
d3=gamma*conj(fft_dy).*fft_dy;
denominator=d1+d2+d3;

img_deblurred=real(ifft2(counter./denominator));
end