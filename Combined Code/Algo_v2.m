function [latent_img] = Algo_v2(kernel, blurred_img, gamma)

% INPUT:
%    - kernel: Point spread function
%    - blurred_img: blurred image 
%    - gamma: gamma value from paper
% OUTPUT:
%    - img_deblurred: deblurred image

%add padding around image
pad_sz          = size(kernel);
%which line works depends on matlab version
blurred_img = edgetaper(blurred_img, kernel);
%blurred_img= padarray(blurred_img,pad_sz,'symmetric');

[n,m]=size(blurred_img);

% Calculate derivatives of kernel
d_x=[1,-1];
d_y=[1;-1];
d_xx=[1,-2,1];
d_yy=[1;-2;1];
d_xy=[1,-1;-1,1]; % d_xy=d_yx?

% initial latent image uses original
%latent_img=blurred_img;

% weights for summing up depending on the derivatives 
w0=50;
w1=25;
w2=12.5;
% Compute the mask
mask = M(blurred_img,kernel);

%Use GPU
% blurred_img=gpuArray(blurred_img);
% kernel_gpu=gpuArray(kernel);
% d_x=gpuArray(d_x);
% d_y=gpuArray(d_y);
% d_xx=gpuArray(d_xx);
% d_yy=gpuArray(d_yy);
% d_xy=gpuArray(d_xy);
% mask=gpuArray(mask);

% Fourier transform everything here to save time later; needs to have right
% dimension
fft_kernel=psf2otf(kernel,[n m]);
%fft_kernel=psf2otf(kernel_gpu,[n m]);
%fft_d=psf2otf(1,[n m]);
%speed_up
fft_d=ones(n,m);
fft_dx=psf2otf(d_x,[n m]);
fft_dy=psf2otf(d_y,[n m]);
fft_dxx=psf2otf(d_xx,[n m]);
fft_dyy=psf2otf(d_yy,[n m]);
fft_dxy=psf2otf(d_xy,[n m]);

% Precompute fouriertransformations for the call of the function Psi
fft_blrd_img=fft2(blurred_img,n,m);
%fft_latent_img=fft_blrd_img;
%fft_m=mask;%fft2(mask,n, m);

%calculate delta
delta0=w0*conj(fft_d).*fft_d;
delta1=w1*conj(fft_dx).*fft_dx+w1*conj(fft_dy).*fft_dy;
delta2=w2*conj(fft_dxx).*fft_dxx+w2*conj(fft_dyy).*fft_dyy+w2*conj(fft_dxy).*fft_dxy;
delta=delta0+delta1+delta2;
%delta=50; %<-improves algo, but should not

%calculate denominator parts
d1=conj(fft_kernel).*fft_kernel.*delta;

%calculate numerator parts; first one doesnt depend on loop
n1=conj(fft_kernel).*fft_blrd_img.*delta;

%initialize for stopping criterion
latent_img=blurred_img;
old_psi_x=inf;
old_psi_y=inf;
old_latent_img=inf;
delta_psi_x=inf;
delta_psi_y=inf;
delta_latent_img=inf;

%%
%Begin loop for optimizing psi and the latent image
%

%TODO: should stop when norm(L) an norm(psi) are belowthreshold, but it
%does not converge atm
for i=1:7
%while (delta_psi_x > 10^-5 || delta_psi_y >10^-5 || delta_latent_img > 10^-5)
gamma=gamma*2;
%calculate psi
[psi_x,psi_y]=Psi(latent_img, gamma, blurred_img, mask);
%psi_x=0;
%psi_y=0;
fft_psi_x=fft2(psi_x,n, m); %this would be more clear, but costs time
fft_psi_y=fft2(psi_y,n, m);

%calculate numerator parts
%first one doesnt depend on loop
%n1=conj(fft_kernel).*fft_blrd_img.*delta;
n2=gamma*conj(fft_dx).*fft_psi_x;
n3=gamma*conj(fft_dy).*fft_psi_y;
numerator=n1+n2+n3;

%calculate denominator parts
%d1=conj(fft_kernel).*fft_kernel.*delta;
d2=gamma*conj(fft_dx).*fft_dx;
d3=gamma*conj(fft_dy).*fft_dy;
denominator=d1+d2+d3;



fft_latent_img=numerator./denominator;
latent_img=real(ifft2(fft_latent_img));

% new_psi_x=psi_x;
% new_psi_y=psi_y;
% 
% delta_psi_x=norm(new_psi_x-old_psi_x);
% delta_psi_y=norm(new_psi_y-old_psi_y);
% delta_latent_img=norm(latent_img-old_latent_img);
% 
% % test1=(new_psi_x==old_psi_x);
% % test2=(new_psi_y==old_psi_y);
% % test3=(latent_img==old_latent_img);
% 
% %imagesc(new_psi_x-old_psi_x);
% %imagesc(new_psi_y-old_psi_y);
% %imagesc(latent_img-old_latent_img);
% %imshow(latent_img)
% %imagesc(new_psi_x)
% %colorbar;
% 
% old_psi_x=new_psi_x;
% old_psi_y=new_psi_y;
% old_latent_img=latent_img;

end

%uses this if padding is used
%latent_img = unpad_image(latent_img,pad_sz);

%imshow(latent_img);
%colorbar;

%GPU version
%latent_img=gather(latent_img);
end