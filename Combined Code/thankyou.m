%% calculates the deblurred Image of our testing images with and without the true kernel

close all

% select a Image out of the set of testimages, i doesn't have to be
% converted
picture = 'tux.jpg';

% the true Kernel has parameter between 25-28 and 88-90 . I can't extract it
% exactly from the Kernelpictures
Kernel_true = fspecial('disk', 4);

dImg=im2double(imread(picture));
%dImg = dImg(:,:,3);

dImg_blurred = imfilter(dImg, Kernel_true,'replicate');
% 
% %
% tic
% [P, kernel] = coarseToFine(dImg_blurred, 9,3,axes);
% toc
% 
% tic
% dImg_deblurred = Algo_v2(Kernel_true,dImg_blurred,2);
% toc
% 
% tic
% P = Algo_v2(kernel, dImg_blurred,200);
% toc

%M1 = M(dImg, Kernel_true);
filename = sprintf('cameraman_deblurred%i.tif',2) ;
imwrite(dImg_blurred,filename);

figure, subplot(1,3,1), subimage(dImg)
subplot(1,3,2), subimage(dImg_blurred)
subplot(1,3,3), subimage(P)
