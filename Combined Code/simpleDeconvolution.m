function [ latent_img ] = simpleDeconvolution( kernel, gamma, delta, fft_blrd_img, fft_dx, fft_dy)
%UNTITLED3 Summary of this function goes here
%   Detailed explanation goes here


% INPUT:
%    - kernel: Point spread function
%    - blurred_img: blurred image 
%    - gamma: gamma value from paper
% OUTPUT:
%    - img_deblurred: deblurred image

%add padding around image
%pad_sz          = 50/2;
%blurred_img             = pad_image(blurred_img,2*pad_sz); 

% Fourier transform the kernel and padd it with zeros
fft_kernel=psf2otf(kernel,size(fft_blrd_img));

%calculate nominator parts
nominator=conj(fft_kernel).*fft_blrd_img.*delta;

%calculate denominator parts
d1=conj(fft_kernel).*fft_kernel.*delta;
d2=gamma*conj(fft_dx).*fft_dx;
d3=gamma*conj(fft_dy).*fft_dy;
denominator=d1+d2+d3;

%uses this if padding is used
%img_deblurred_padded=real(ifft2(nominator./denominator));
%img_deblurred = unpad_image(img_deblurred_padded,2*pad_sz);

latent_img=real(ifft2(nominator./denominator));

end

