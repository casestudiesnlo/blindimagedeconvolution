close all
%-----------------------------------
% Set Parameters
%-----------------------------------

% choose picture
picture = 'cameraman.tif';
% set degree of noise
percnoise = 0.01;
% choose the kind of kernel of the deblurred picture (here hardcoded)
% set 1 for out of focus, 2 for Gaussian and 3 for motion
psf_choice = 1;

% Set parameters of the point spread function, these are currently
% hardcoded
switch psf_choice    
    case 1 
        [PSFtilde, center] = psfDefocus([11,11],4);
    case 2      
        [PSFtilde,center] = psfGauss([5,5],2);
    case 3         
        load motionpsf3, center = [12,11];
        PSFtilde = ps;
end

% transform the picture into a matrix
dImg=im2double(imread(picture)); 
dImg = dImg(:,:,1);


% set size of the matrix
[n,m]=size(dImg);

PSF_org=PSFtilde;

%pixel(1,1) will be used as center
PSF=padPSF(PSFtilde,size(dImg));
% circleshift the PSA, so the picture is not moved
PSFtilde=circshift(PSF,1-center);

%-----------------------------------
% BLURR PICTURE
%-----------------------------------
% calculate the eigenvalues of the point spread function
%eigA=fft2(PSFtilde,size(dImg,1),size(dImg,2));
eigA=psf2otf(PSF_org,[size(dImg,1) size(dImg,2)]);
b=real(ifft2(eigA.*fft2(dImg))); 
% add gaussian noise to the picture
randn('seed',27)
e=randn(size(b));
e=e/norm(e(:));
e=percnoise*e*norm(b(:));
dImg=b+e; 

% calculate the norm of the original picture to compare it to the 
% reconstructed one
normx=norm(dImg(:));


%--------------------------------------
% IMAGE DEBLURRING
%--------------------------------------

%---------------------------------------
% Start the deblurring algorithm here
tic;
[dImg_deb] = Algo_v2(PSF_org, dImg,2);
toc;


%--------------------------------------
% OUTPUT
%--------------------------------------

%Show the accuracy
RRE_deb = norm(dImg_deb(:)-dImg(:))/normx;
disp(RRE_deb)


% save the blurred image
imwrite(uint8(dImg),'cameraman_blurred.tif');
% save the deblurred image
imwrite(uint8(dImg_deb),'cameraman_deblurred.tif');
