function [ psi_x, psi_y ] = Psi(latent_img, gamma, blrd_img, mask)
%Psi calculates the parameter psi for the Non-Blind Deconvolution
%   We need the Kernel in the input, to compute the Mask.
%   Alternatively we could compute the mask outside, and give it as Input.
%%
% Define constants

[n,m]=size(blrd_img);

a=6.1*10^-4;
b=5;
k=2.7;
l=1.852627281;

%d_x=[1,-1];
%d_y=[1;-1];

%guessed a value from the range
lambda_1=0.01; %range: 0.002-0.5
lambda_2=15; %range: 10-25

%fft_dvar(1,:,:)=fft_dx;%psf2otf(d_x,[n m]);
%fft_dvar(2,:,:)=fft_dy;%psf2otf(d_y,[n m]);


left_borders=[0,-l,-inf,-l];
right_borders=[l,0,l,inf];

% TODO: values at infinity are not needed, remove them

PSI   = zeros(2,n,m);

[d_blurred(1,:,:),d_blurred(2,:,:)] = imgradientxy(blrd_img,'intermediate');
[d_latent(1,:,:),d_latent(2,:,:)] = imgradientxy(latent_img,'intermediate');

for i=1:2
%%
% Start psi_var_now calculations

d_blrd_img=squeeze(d_blurred(i,:,:));
d_latent_img=squeeze(d_latent(i,:,:));

n1=lambda_1*k;
n2=lambda_2*mask.*d_blrd_img;
n3=gamma*d_latent_img;

d1=lambda_2*mask+gamma;
d2=lambda_1*a;

%3 different psi values
psi_1_var_now=(-n1+2*n2+2*n3) ./ (2*(d1));
psi_2_var_now=(n1+2*n2+2*n3)./(2*(d1));
psi_3_var_now=(n2+n3)./(d2+d1);

%transform back from fft so it can be compared with borders
%psi_1_var_now_ifft=psi_1_var_now;%real(ifft2(psi_1_var_now));
%psi_2_var_now_ifft=psi_2_var_now;%real(ifft2(psi_2_var_now));
%psi_3_var_now_ifft=psi_3_var_now;%real(ifft2(psi_3_var_now));

%check if psi is in intervals
%using intervals as (left_border,right_border]

left_border_checker=permute(repmat(left_borders,m,1,n),[3,1,2]);
is_higher=cat(3, psi_1_var_now,psi_2_var_now,psi_3_var_now,psi_3_var_now)>left_border_checker;

right_border_checker=permute(repmat(right_borders,m,1,n),[3,1,2]);
is_lower=cat(3, psi_1_var_now,psi_2_var_now,psi_3_var_now,psi_3_var_now)<=right_border_checker;

in_borders=((is_higher+is_lower)==2);


%Energy in Interval
ep1_1=n1*psi_1_var_now;
ep2_1=lambda_2*mask.*(psi_1_var_now-d_blrd_img).^2+gamma.*(psi_1_var_now-d_latent_img).^2;

ep1_2=n1*psi_2_var_now;
ep2_2=lambda_2*mask.*(psi_2_var_now-d_blrd_img).^2+gamma.*(psi_2_var_now-d_latent_img).^2;

ep1_3=lambda_1*(a*(psi_3_var_now).^2+b);
ep2_3=lambda_2*mask.*(psi_3_var_now-d_blrd_img).^2+gamma.*(psi_3_var_now-d_latent_img).^2;


energy_1_var_now_int= ep1_1+ep2_1 ;
energy_2_var_now_int= -ep1_2+ep2_2;
energy_3_var_now_int= ep1_3+ep2_3 ;

%Energy at borders
%use left border and at last interval also right border as they are the
%same

%Energy at left borders
ep1_1_l=n1*0;
ep2_1_l=lambda_2*mask.*(0-d_blrd_img).^2+gamma.*(0-d_latent_img).^2;

ep1_2_l=n1*(-l);
ep2_2_l=lambda_2*mask.*(-l-d_blrd_img).^2+gamma.*(-l-d_latent_img).^2;

%speedup inf
%ep2_3_l_1=(lambda_2*fft_m+gamma).*(-inf-fft_dvar_now.*fft_blrd_img).^2;
%ep3_3_l_1=lambda_1*(a*(-inf)^2+b);

ep2_3_l_2=lambda_2*mask.*(l-d_blrd_img).^2+gamma.*(l-d_latent_img).^2;
ep3_3_l_2=lambda_1*(a*(l)^2+b);


energy_1_var_now_l=ep1_1_l+ep2_1_l  ;
energy_2_var_now_l= -ep1_2_l+ep2_2_l;
%speedup inf
%energy_3_var_now_l_1=real(ifft( ep3_3_l_1+ep2_3_l_1 ));
%energy_3_var_now_l_1=inf;
energy_3_var_now_l_2= ep3_3_l_2+ep2_3_l_2 ;

%Energy at right borders
ep1_1_r=n1*l;
ep2_1_r=lambda_2*mask.*(l-d_blrd_img).^2+gamma.*(l-d_latent_img).^2;

ep1_2_r=n1*0;
ep2_2_r=lambda_2*mask.*(0-d_blrd_img).^2+gamma.*(0-d_latent_img).^2;

ep2_3_r_1=lambda_2*mask.*(-l-d_blrd_img).^2+gamma.*(-l-d_latent_img).^2;
ep3_3_r_1=lambda_1*(a*(-l)^2+b);
%speedup inf
%ep2_3_r_2=(lambda_2*fft_m+gamma).*(inf-fft_dvar_now.*fft_blrd_img).^2;
%ep3_3_r_2=lambda_1*(a*(inf)^2+b);

energy_1_var_now_r= ep1_1_r+ep2_1_r  ;
energy_2_var_now_r=-ep1_2_r+ep2_2_r ;
energy_3_var_now_r_1=ep3_3_r_1+ep2_3_r_1 ;
%speedup inf
%energy_3_var_now_r_2=real(ifft( ep3_3_r_2+ep2_3_r_2 ));

%Caluclate the minima, if not in interval use minima of borders
[border_min_1_var_now,border_min_ind]=min(cat(3,energy_1_var_now_l,energy_1_var_now_r),[],3);
energy_1_var_now=energy_1_var_now_int.*in_borders(:,:,1)+border_min_1_var_now.*(~in_borders(:,:,1));
psi_1_var_now=psi_1_var_now.*in_borders(:,:,1)+(~in_borders(:,:,1)).*((border_min_ind==1).*left_borders(1)+(border_min_ind==2).*right_borders(1));

[border_min_2_var_now,border_min_ind]=min(cat(3,energy_2_var_now_l,energy_2_var_now_r),[],3);
energy_2_var_now=energy_2_var_now_int.*in_borders(:,:,2)+border_min_2_var_now.*(~in_borders(:,:,2));
psi_2_var_now=psi_2_var_now.*in_borders(:,:,2)+(~in_borders(:,:,2)).*((border_min_ind==1).*left_borders(2)+(border_min_ind==2).*right_borders(2));

%speedup inf
%[border_min_3_var_now_1,border_min_ind]=min(cat(3,energy_3_var_now_l_1,energy_3_var_now_r_1),[],3);
energy_3_var_now_1=energy_3_var_now_int.*in_borders(:,:,3)+energy_3_var_now_r_1.*(~in_borders(:,:,3));
psi_3_var_now=psi_3_var_now.*in_borders(:,:,3)+(~in_borders(:,:,3)).*right_borders(3);
%speedup inf
%psi_3_var_now=psi_3_var_now.*in_borders(:,:,3)+(~in_borders(:,:,3)).*((border_min_ind==2).*right_borders(3));
%Is this never the case, that left_border is the min? If this is the case
%implement another check as this function tries 0*-inf and so produces NaN
%psi_3_var_now=psi_3_var_now.*in_borders(:,:,3)+(~in_borders(:,:,3)).*((border_min_ind==1).*left_borders(3)+(border_min_ind==2).*right_borders(3));

%speedup inf
%[border_min_3_var_now_2,border_min_ind]=min(cat(3,energy_3_var_now_l_2,energy_3_var_now_r_2),[],3);
energy_3_var_now_2=energy_3_var_now_int.*in_borders(:,:,4)+energy_3_var_now_l_2.*(~in_borders(:,:,4));
psi_3_var_now=psi_3_var_now.*in_borders(:,:,4)+(~in_borders(:,:,4)).*left_borders(4);
%Is this never the case, that right_border is the min? If this is the case
%implement another check as this function tries 0*inf and so produces NaN
%psi_3_var_now=psi_3_var_now.*in_borders(:,:,4)+(~in_borders(:,:,4)).*((border_min_ind==1).*left_borders(4)+(border_min_ind==2).*right_borders(4));

%get the index of the minima
[~,real_ind]=min(cat(3, energy_1_var_now,energy_2_var_now,energy_3_var_now_1,energy_3_var_now_2),[],3);

%Calculate which of the 4 different psi is the minimum and output this

%get gpu_array back to Matlab
% real_ind=gather(real_ind); 
% if (real_ind==1)
%     PSI(i,:,:)=psi_1_var_now;
% end
% if (real_ind==2)
%     PSI(i,:,:)=psi_2_var_now;
% end
% if (real_ind==3)
%     PSI(i,:,:)=psi_3_var_now;
% end
% if (real_ind==4)
%     PSI(i,:,:)=psi_3_var_now;
% end

%imagesc(real_ind);

PSI(i,:,:)=(real_ind==1).*psi_1_var_now+(real_ind==2).*psi_2_var_now+(real_ind==3).*psi_3_var_now+(real_ind==4).*psi_3_var_now;
end

psi_x=squeeze(PSI(1,:,:));
psi_y=squeeze(PSI(2,:,:));

end

