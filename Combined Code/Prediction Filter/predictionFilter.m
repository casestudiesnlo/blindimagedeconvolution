function [dx, dy] = predictionFilter(blurred_img, sigma_s, sigma_r, dt, r, k, m)
%predictionFilter Applies a filter to the blurred image in order to
%reconstruct the edges

% INPUT:
%    - kernel: Point spread function
%    - blurred_img: blurred image, which should be filtered 
%    - sigma_s: spatial parameter for the filter, default value is 2 
%    - sigma_r: range parameter for the filter (should be high for noisy input images), 
%       default value is 0.5 
%    - dt: time step width, default value is 1
%    - r: treshold parameter for calculating the truncating gradients 
%    - k: number of iteration in the blind deconvolution algorithm
%    - m: kernelSize
% OUTPUT:
%    - filtered_img: filtered image
%   
%   See paper [1] Fast Motion Deblurring, page 5, chaper 4,
%   part prediction for more details and explanations to the method
% -> You can test the method by calling the function with default values
% "predictionFilter(im2double(imread('cameraman.tif')),2,0.5,1,2,1,9);"

% -------------------------------------------------------------------------
if max(blurred_img(:)) > 1
    % Convert the picture into double precision with values in [0,1] only
    blurred_img = blurred_img(:,:,1)/255;
end
%[~,m] = size(blurred_img);
% -------------------------------------------------------------------------
% FIRST APPLY BILTERAL FILTER (see wikipedia for more details)
if k <= 7 % the fast method only works for big values of sigma_r
    %filtered_img = BilateralFast(blurred_img,sigma_s,sigma_r*0.9^(k-1));
else % the slow method works for all values of sigma_r
    %filtered_img = BilateralSlow(blurred_img,sigma_s,sigma_r*0.9^(k-1)); 
end
filtered_img = blurred_img;
% -------------------------------------------------------------------------
% APPLY SHOCK FILTER (see paper for more details)
% we implement the equation: output = input - sign(laplacian(input).* ||gradient(u)||*dt
[gx, gy]=gradient(filtered_img);
filtered_img = filtered_img - sign(del2(filtered_img)).*sqrt(gx.*gx+gy.*gy).*dt*0.9^(k-1);
    
% -------------------------------------------------------------------------
% APPLY GRADIENT MAP FILTERING (see paper and modelling folder for more details)    

% calculating the gradient map of an image is cheaper using the matlab
% function than ffts, we could consider changing this in our other
% implementations as well. 
%dx = ifft2(fft2(filtered_img).*psf2otf([-1,1],[n m]));
%dy = ifft2(fft2(filtered_img).*psf2otf([-1;1],[n m]));
[dx,dy] = imgradientxy(filtered_img,'intermediate');
% now we compute the matrix of gradient directions a_i,j = arctan(dx_i,j/dy_i,j)
% and normalizing the matrix as multiples of 45� a_i,j = floor(a_i,j mod 4/pi)* pi/4
% Here we used that vectors with opposite orientation have the same
% direction, hence we get only 4 kinds of directions.
A = floor(mod(atan(dx./dy),pi)*4/pi)*pi/4;
% As a next step we calculate the gradient magnitudes of each direction
M = sqrt(dx.^2+dy.^2);
% We count now the amount of the 4 directions we get in A. 
[~,~,bin] = histcounts(A,4);
% For each of the four directions we sort the part of M, that belongs to 
% the current direction, descending and define the corresponding threshhold 
% as the r*m-th value in the sorted vector 

% r*m, is never scaled
rm = floor(r*m); 
thresh1 = sort(M(bin==1),'descend');
thresh1 = thresh1(rm); 
thresh2 = sort(M(bin==2),'descend');
thresh2 = thresh2(rm); 
thresh3 = sort(M(bin==3),'descend');
thresh3 = thresh3(rm); 
thresh4 = sort(M(bin==4),'descend');
thresh4 = thresh4(min(size(thresh4,1), rm)); 
% If we have done this, we take the minimum over all these 4 thresholds and
% scale according to iteration
thresh = min([thresh1,thresh2,thresh3,thresh4]) *0.9^(k-1); 
% and set values dx_i,j = 0, if m_ij < threshold
M = find(M<thresh);
dx(M) = 0;
dy(M) = 0;
% For testing save the image as a tif
% imwrite(uint8(255*filtered_img),'cameraman_filtered.tif');

end

