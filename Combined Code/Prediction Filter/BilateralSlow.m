function [ filtered_img ] = BilateralSlow(blurred_img, sigma_s, sigma_r)
%UNTITLED3 Summary of this function goes here
%   Detailed explanation goes here

[n,m] = size(blurred_img);
w=5;
[X,Y] = meshgrid(-w:w,-w:w);
G = exp(-(X.^2+Y.^2)/(2*sigma_s^2));
filtered_img = zeros(n,m);
for j = 1:m
   for i = 1:n
         % Extract local region.
         iMin = max(i-w,1);
         iMax = min(i+w,n);
         jMin = max(j-w,1);
         jMax = min(j+w,m);
         I = blurred_img(iMin:iMax,jMin:jMax);
         % Compute the pixel weights using a Gaussian distribution
         H = exp(-(I-blurred_img(i,j)).^2/(2*(sigma_r)^2));
         % Calculate bilateral filter response.
         F = H.*G((iMin:iMax)-i+w+1,(jMin:jMax)-j+w+1);
         filtered_img(i,j) = sum(F(:).*I(:))/sum(F(:));
   end
end
end

