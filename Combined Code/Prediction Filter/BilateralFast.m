function [ filtered_img , param ]  =  BilateralFast(blurred_img, sigma_s, sigma_r)
[n, m]   =  size(blurred_img);
fnum  =  zeros(n, m);
fdenom  =  zeros(n, m);
filtered_img  =  zeros(n, m);
ii = sqrt(-1);
param.method = 0;
% create spatial filter
w  = 5; if (mod(w,2) == 0); w  = w+1; end
filt     = fspecial('gaussian', [w w], sigma_s);
% compute local dynmaic range
T  =  maxFilter(blurred_img, w);
ratio = T/sigma_r;
if (ratio > 3.5)
    param.method = 1;
    eps = 1e-3;
    K = T+1;
    w0 = pi/T;
    samples = (1:K)';
    b = exp(-0.5*(samples-1).^2/sigma_r^2);
    n = 1;
    a = cos((n-1)*w0*(samples-1));
    R = norm(a);
    c = []; % fix for c is undefined
    Q = a/R;
    A = a;
    proj = Q'*b;
    residual = norm(b - Q*proj);
    while residual > eps
        n = n+1;
        rho = zeros(n,1);
        q = cos((n-1)*w0*(samples-1));
        A = [A, q];
        for j = 1 : n-1
            % new q and rho
            qm = Q(:,j);
            rho(j) = qm'*q;
            q = q - rho(j)*qm;
        end
        rho(n) =norm(q);
        q = q/rho(n);
        Q = [Q, q];
        proj = [proj; q'*b];
        R = [R; zeros(1,n-1)];
        R = [R, rho]; %#ok<*AGROW>
        c = backSub(R,proj,n);
        residual = [residual, norm(b-A*c)];
        % save parameters
        param.T  = T;
        param.N  = n-1;
        param.coeff = c;
    end
    w0  = pi/T;
    L = length(c);
    for k = 1 : L
        alpha = 0.5*c(k);
        % positive frequency
        H  = exp(ii*(k-1)*w0*blurred_img);
        G  = conj(H);
        F  =  G.*blurred_img;
        barF  = imfilter(F, filt, 'symmetric');
        barG = imfilter(G, filt, 'symmetric');
        fnum =  fnum + alpha * (H .* barF);
        fdenom  = fdenom + alpha * (H .* barG);
        % negative frequency
        H  = exp(-ii*(k-1)*w0*blurred_img);
        G  = conj(H);
        F  =  G.*blurred_img;
        barF  = imfilter(F, filt, 'symmetric');
        barG = imfilter(G, filt,'symmetric');
        fnum =  fnum + alpha * (H .* barF);
        fdenom  = fdenom + alpha * (H .* barG);
    end
else
    N  =  4*ceil(0.405*(T/sigma_r)^2 );
    gamma   =  1 / (sqrt(N) * sigma_r);
    twoN     =  2^N;
    for k = 0: N
        omegak = (2*k - N)*gamma;
        bk = nchoosek(N,k) / twoN;
        H  = exp(-ii*omegak*blurred_img);
        G  = conj(H);
        F  = G.*blurred_img;
        barF  = imfilter(F, filt);
        barG = imfilter(G, filt);
        fnum =  fnum + bk * H .* barF;
        fdenom  = fdenom + bk * H .* barG;
    end
    % save parameters
    param.T  = T;
    param.N  = N;
end
% check for division by small numbers
idx1 = find( fdenom < 1e-3);
idx2 = find( fdenom > 1e-3);
filtered_img( idx1 ) = blurred_img( idx1 );
filtered_img( idx2 ) = real(fnum( idx2 )./fdenom (idx2 ));
end

% This function is implemented by Kunal Chaudhury 2012 and updated 2016




