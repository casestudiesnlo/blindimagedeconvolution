function [x] = Tik(PSF, b)

% INPUT:
%     - eigA: eigenvalues of the blur operator A
%     - eigLstarL: eigenvalues of the matrix L'L
%     - b: observed image
% OUTPUT:
%     - x: Tikhonov solution computed with parameter "alpha_Tik"
%     - xhat: solution in the Fourier domain

[n,m]=size(b);

L=zeros(size(b));
L(1:3,1:3)=[0 -1 0; -1 4 -1; 0 -1 0]/8;
centrL=[2,2];
eigLstarL =fft2(circshift(L,1-centrL));

eigA=fft2(PSF, n, m);
%eigA=psf2otf(PSF,[n m]);
%eigLstarL=ones(size(b));


bhat=fft2(b);
B=conj(eigA).*bhat;

alpha_Tik = gcv_sparse(eigA(:), bhat(:),eigLstarL(:));

xhat=B./((abs(eigA).^2)+alpha_Tik*eigLstarL);

x=real(ifft2(xhat));
end