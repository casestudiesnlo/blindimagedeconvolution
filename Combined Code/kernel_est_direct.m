function [ kernel ] = kernel_est_direct( Px, Py, kernel, fft_blrd_img, fft_dx, fft_dy, fft_dxx, fft_dyy, fft_dxy )
%kernel_est Directly estimates a kernel from a blurred image
%   beta    Thikonov regularization, it is set to 5
%   blurred_image is initial blurred image we are working with
%   Px latent image x gradient 
%   Py latent image y gradient
%   kernelSize assumed size of kernel
%   in the best case, this should be the sharp image we wanna
%   find, but since the kernel estimation has to be done from the very start
%   we are working here with a filtered and slightly sharpened version of
%   the blurred image. Here, we get the gradients [Px, Py] of the latent image
%
%   Format of the images: the input should look like
%   double(imread('image')); if it is a colored image, convert it into a
%   grayscale-image

beta=5;
%[n,m]=size(fft_blrd_img);
kernelSize = size(kernel, 1);

%   (P*,B*) the partial derivatives of the blurred image and the latent
%           sharp image.%           
%
%   w*      {w1,w2} weights for each partial derivative given in [Shan et
%   al. 2008][7] 
%           weights for summing up depending on the derivatives

%   QUESTION:
%   The following is the same as in Algo_v2. Do we have to do it in every new
%   function? Should we write it in main.m?

% d_x=[1,-1];
% d_y=[1;-1];
% d_xx=[1,-2,1];
% d_yy=[1;-2;1];
% d_xy=[1,-1;-1,1]; % d_xy=d_yx?

% pad array to prevent boundary artifaces
% pad = 0; 
% padX = pad*(kernelSize+1); % todo calculate length, n+padX should be power of prime numbers 
% padY = pad*(kernelSize+1);
% np = n+padX;
% mp = m+padX;
% 
% Px = padarray(Px, [padX/2 padY/2], 0); 
% Py = padarray(Py, [padX/2 padY/2], 0);

% TODO should this be padded the same way?
% blurred_img = padarray(blurred_img, [padX/2 padY/2], 0);
% 
% fft_blrd_img=fft2(blurred_img);
 fft_P_x=fft2(Px);
 fft_P_y=fft2(Py);
% 
% fft_dx=psf2otf(d_x,[np mp]);
% fft_dy=psf2otf(d_y,[np mp]);
% fft_dxx=psf2otf(d_xx,[np mp]);
% fft_dyy=psf2otf(d_yy,[np mp]);
% fft_dxy=psf2otf(d_xy,[np mp]);

w1=25;
w2=12.5;

%%
%   Precompute: Implementation of (P*,B*)

% note: for P, we already have the first-order derivatives
fft_P_xx=fft_P_x.*fft_dx;
fft_P_yy=fft_P_y.*fft_dy;
fft_P_xy=fft_P_x.*fft_dy;
fft_P_yx=fft_P_y.*fft_dx;
fft_P_diag = (fft_P_xy + fft_P_yx) / 2;

fft_B_x=fft_blrd_img.*fft_dx;
fft_B_y=fft_blrd_img.*fft_dy;
fft_B_xx=fft_blrd_img.*fft_dxx;
fft_B_yy=fft_blrd_img.*fft_dyy;
fft_B_xy=fft_blrd_img.*fft_dxy;

%%
%   Precumpute: b^bar=A^T*b

fft_b_x=conj(fft_P_x).*fft_B_x;
fft_b_y=conj(fft_P_y).*fft_B_y;
fft_b_xx=conj(fft_P_xx).*fft_B_xx;
fft_b_yy=conj(fft_P_yy).*fft_B_yy;
fft_b_xy=conj(fft_P_diag).*fft_B_xy;

delta=w1*fft_b_x+w1*fft_b_y+w2*fft_b_xx+w2*fft_b_yy+w2*fft_b_xy;


%%
%   Precumpute: (A^T*A)^head
a_x=conj(fft_P_x).*fft_P_x;
a_y=conj(fft_P_y).*fft_P_y;
a_xx=conj(fft_P_xx).*fft_P_xx;
a_yy=conj(fft_P_yy).*fft_P_yy;
a_diag=conj(fft_P_diag).*fft_P_diag;

A_t_A_h=w1*a_x+w1*a_y+w2*a_xx+w2*a_yy+w2*a_diag;

%%
%   Solution of k 

kernel_pad=real(ifft2(delta./(A_t_A_h+beta)));


%% The kernel has to be adjusted. Values that are 1/20 smaller than the biggest entry are set to zero
% First step detects the maximal entry of the kernel and substract 1/20 of
% this value from our Matrix kernel --> entries which were smaller than this
% value, now are smaller than 0.

% max(A,B) creates a Matrix which takes the bigger values from eather A and
% B. In our case the entries smaller than 0 are set to 0. And the entries
% bigger than 0 are unchanged.
kernel = extract_kernel(kernel_pad, kernelSize);


kernel=max(kernel-(1/20)*max(kernel(:)),0);

% logical index. seems to be slower
% kernel(kernel<(1/20)*max(kernel(:))) = 0;


% normalize the kernel such that it sums up to 1.
kernel=kernel./(sum(kernel(:)));

end