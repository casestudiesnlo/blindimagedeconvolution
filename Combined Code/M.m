function [mask] = M(blurred_img, kernel)
% M calculates the Mask (M) for the Non-Blind Deconvolution

t = 0.02;
[k, l] = size(kernel);
img_sqrd = blurred_img.^2;
img_scaled = blurred_img/(k*l);
kernel_M = ones(k,l);
% Calculate the standard deviation of the image
% Convolute the squared image with the kernel
mask1 = conv2(img_sqrd,kernel_M,'same');
% Convolute the scaled image with the kernel
mask2 = conv2(img_scaled,kernel_M,'same');
% Add/combine both convolutions to calculate the standard deviation
mask = sqrt(mask1/(k*l) - mask2.^2);
mask = double(mask<t);

end