function [x,mask] = pad_image(I,pad_size,method)
% method == 1 -> constant padding
% method == 2 -> zero padding
if (nargin == 2)
    method = 1;
end

if (length(pad_size) == 1)
    pad_size = [pad_size,pad_size];
end

[n,m]=size(I);
hfs_x1=floor((pad_size(2)-1)/2);
hfs_x2=ceil((pad_size(2)-1)/2);
hfs_y1=floor((pad_size(1)-1)/2);
hfs_y2=ceil((pad_size(1)-1)/2);

% Construct mask for extending the image (constant edges)
m=m+hfs_x1+hfs_x2;
n=n+hfs_y1+hfs_y2;
mask=zeros(n,m);
mask(hfs_y1+1:n-hfs_y2,hfs_x1+1:m-hfs_x2)=1;

tI=I;
x=tI([ones(1,hfs_y1),1:end,end*ones(1,hfs_y2)],[ones(1,hfs_x1),1:end,end*ones(1,hfs_x2)]);
if (method ==2)
   x(~mask) = 0; 
end
end
