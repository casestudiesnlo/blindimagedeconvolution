function varargout = final_presentation(varargin)
% FINAL_PRESENTATION MATLAB code for final_presentation.fig
%      FINAL_PRESENTATION, by itself, creates a new FINAL_PRESENTATION or raises the existing
%      singleton*.
%
%      H = FINAL_PRESENTATION returns the handle to a new FINAL_PRESENTATION or the handle to
%      the existing singleton*.
%
%      FINAL_PRESENTATION('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in FINAL_PRESENTATION.M with the given input arguments.
%
%      FINAL_PRESENTATION('Property','Value',...) creates a new FINAL_PRESENTATION or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before final_presentation_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to final_presentation_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help final_presentation

% Last Modified by GUIDE v2.5 07-Jul-2016 19:02:22

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @final_presentation_OpeningFcn, ...
                   'gui_OutputFcn',  @final_presentation_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before final_presentation is made visible.
function final_presentation_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to final_presentation (see VARARGIN)

%Initialize view

%load placeholder
handles.placeholder = cell(2,1);
handles.placeholder = im2double(imread('images/placeholder.png'));
imshow(imread('images/placeholder.png'));

axes(handles.axes1);
imshow(handles.placeholder);

handles.images = cell(2,1);

%Set initial Images
%picture{1} = 'Cameraman';
picture{1} = 'QR-code';
picture{2} = 'Voltaren';
picture{3} = 'Brief';
picture{4} = 'Tux';
picture{5} = 'End';

set(handles.popupmenu1,'String',picture);

handles.images = cell(2,1);
% load images
%handles.images{1} = im2double(imread('images/blind/cameraman_blurred.tif'));
handles.images{1} = im2double(imread('images/blind/qr-code-mvtec.png'));
handles.images{2} = im2double(imread('images/blind/voltaren.png'));
handles.images{3} = im2double(imread('images/blind/brief.png'));
handles.images{4} = im2double(imread('images/blind/tux_blurred.tif'));
handles.images{5} = im2double(imread('images/blind/thankYou_blurred.tif'));


handles = setImage(handles, 1);


% Choose default command line output for final_presentation
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes final_presentation wait for user response (see UIRESUME)
% uiwait(handles.figure1);
function handles = setImage(handles, index)
% sets current Image to image specified by index
% updates handles.img property accordingly
% shows image on the left
handles.imgIndex = index;
handles.img = handles.images{handles.imgIndex};
axes(handles.axes1);
imshow(handles.img);

function handles = deblurImg(handles)
% the true Kernel has parameter between 25-28 and 88-90 . I can't extract it
% exactly from the Kernelpictures
Kernel_true = fspecial('motion', 97, 49);

dImg_blurred_org=handles.img;
dImg_blurred=dImg_blurred_org;

%check if image is colored or grey
if(ndims(dImg_blurred_org)==3)
    dImg_blurred=rgb2gray(dImg_blurred_org);
end
%
kernel_size=29;
if(handles.imgIndex==5)
    kernel_size=99;
end


tic
[P, kernel] = coarseToFine_final(dImg_blurred, kernel_size,2, handles.axes2);
toc

% tic
% dImg_deblurred = Algo_v2(Kernel_true,dImg_blurred,2);
% toc
% if(handles.imgIndex==5)
%     kernel=Kernel_true;
% end

if(ndims(dImg_blurred_org)<3)
    tic
    dImg_Deb = Algo_v2(kernel, dImg_blurred,2);
    toc
end

if(ndims(dImg_blurred_org)==3)
    for i=1:3
        dImg_blurred=dImg_blurred_org(:,:,i);

        tic
        dImg_Deb(:,:,i) = Algo_v2(kernel, dImg_blurred,2);
        toc
    end
end

axes(handles.axes1);
imshow(dImg_Deb);


% --- Outputs from this function are returned to the command line.
function varargout = final_presentation_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on selection change in popupmenu1.
function popupmenu1_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns popupmenu1 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu1
% Determine the selected data set.
val = get(hObject,'Value');
% Set current data to the selected data set.
handles = setImage(handles, val);
% Save the handles structure.
guidata(hObject,handles)

% --- Executes during object creation, after setting all properties.
function popupmenu1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pushbutton1.
function pushbutton1_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
deblurImg(handles);
