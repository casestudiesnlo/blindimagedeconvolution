function [ L ] = Algo_v1_d(f, I, gamma, omega, psi_x, psi_y)
%OPTL (working title) Updated sharp image L
%
%REMEMBER TO UPDATE THIS AT THE END
%function [ L ] = OPTL(f, I, gamma, psi, tau)
%
%   L       (Matrix mxn) Latent sharp image we want to accomplish
%   f       (Matrix kxl) blur-kernel (is given for non-blind deconvolution)
%   I       (Matrix mxn) given blurred Image
%   gamma   (constant Number) controls how strongly psi is constrained to 
%           be similar to the gradient of L
%   
%
%   omega   tau is some constant for the calculation of omega. It apears in the
%   computation of the delta in den non-blind deconvolution on page 73:6 in
%   the "High-Quality Motion Deblurring from a Single Image
%
%
%   psi     computed approximation of the gradient of L (default = 0
%           because we don't know it yet)
%   
%   Should we write some "database" for the kernels of the derivation?
%   
%   size of the picture  
[n,m]=size(I);

%   Look at the parameter psi
if nargin < 5
  psi_x = 0;
  psi_y = 0;
end

%   Load the derivative matrices
load('derivatives.mat');

%   weights for the derivatives to calculate Delta. 
%   It expresses the influence of the derivatives.
%   In the paper these parameters are set to 50, 25, 12.5. But they can be
%   choosen individually -> w0=c; w1=1/2*w0; w2=1/2*w1
w0=omega;
w1=1/2*w0;
w2=1/2*w1;

% Fourier transform everything here to save time later; needs to have right
% dimension
fft_blrd_img=fft2(I,n, m);
fft_kernel=psf2otf(f,[n m]);
fft_psi_x=psf2otf(psi_x,[n m]);
fft_psi_y=psf2otf(psi_y,[n m]);
fft_d=psf2otf(1,[n m]);
fft_dx=psf2otf(thetax1,[n m]);
fft_dy=psf2otf(thetay1,[n m]);
fft_dxx=psf2otf(thetax2,[n m]);
fft_dyy=psf2otf(thetay2,[n m]);
fft_dxy=psf2otf(thetaxy2,[n m]);

%calculate delta
delta0=w0*conj(fft_d).*fft_d;
delta1=w1*conj(fft_dx).*fft_dx+w1*conj(fft_dy).*fft_dy;
delta2=w2*conj(fft_dxx).*fft_dxx+w2*conj(fft_dyy).*fft_dyy+w2*conj(fft_dxy).*fft_dxy;
delta=delta0+delta1+delta2;

%calculate counter parts
c1=conj(fft_kernel).*fft_blrd_img.*delta;
c2=gamma*conj(fft_dx).*fft_psi_x;
c3=gamma*conj(fft_dy).*fft_psi_y;
counter=c1+c2+c3;

%calculate denominator parts
d1=conj(fft_kernel).*fft_kernel.*delta;
d2=gamma*conj(fft_dx).*fft_dx;
d3=gamma*conj(fft_dy).*fft_dy;
denominator=d1+d2+d3;

L=real(ifft2(counter./denominator));
end