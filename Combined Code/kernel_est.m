function [ kernel ] = kernel_est(Px, Py, kernel, fft_blrd_img, fft_dx, fft_dy, fft_dxx, fft_dyy, fft_dxy )
%kernel_est Estimates a blurr kernel out of a blurred image. 
%   beta    Thikonov regularization, it is set to 5
%   blurred_image is initial blurred image we are working with
%   Px latent image x gradient 
%   Py latent image y gradient
%   in the best case, this should be the sharp image we wanna
%   find, but since the kernel estimation has to be done from the very start
%   we are working here with a filtered and slightly sharpened version of
%   the blurred image. Here, we get the gradients [Px, Py] of the latent image
%
%   Format of the images: the input should look like
%   double(imread('image')); if it is a colored image, convert it into a
%   grayscale-image

MAX_CG_ITER = 100;
CG_STOP_CRIT = 0.01;

beta=5;
[n, m]=size(Px);

%   (P*,B*) the partial derivatives of the blurred image and the latent
%           sharp image.%           
%
%   w*      {w1,w2} weights for each partial derivative given in [Shan et
%   al. 2008][7] 
%           weights for summing up depending on the derivatives

fft_P_x=fft2(Px);
fft_P_y=fft2(Py);

w1=25;
w2=12.5;

%%
%   Precompute: Implementation of (P*,B*)

% note: for P, we already have the first-order derivatives
fft_P_xx=fft_P_x.*fft_dx;
fft_P_yy=fft_P_y.*fft_dy;
fft_P_xy=fft_P_x.*fft_dy;
fft_P_yx=fft_P_y.*fft_dx;
fft_P_diag = (fft_P_xy + fft_P_yx) / 2;

fft_B_x=fft_blrd_img.*fft_dx;
fft_B_y=fft_blrd_img.*fft_dy;
fft_B_xx=fft_blrd_img.*fft_dxx;
fft_B_yy=fft_blrd_img.*fft_dyy;
fft_B_xy=fft_blrd_img.*fft_dxy;

%%
%   Precompute: b^bar=A^T*b

fft_b_x=conj(fft_P_x).*fft_B_x;
fft_b_y=conj(fft_P_y).*fft_B_y;
fft_b_xx=conj(fft_P_xx).*fft_B_xx;
fft_b_yy=conj(fft_P_yy).*fft_B_yy;
fft_b_xy=conj(fft_P_diag).*fft_B_xy;

delta=w1*fft_b_x+w1*fft_b_y+w2*fft_b_xx+w2*fft_b_yy+w2*fft_b_xy;
b_bar=real(ifft2(delta));

% exchanging ifft2 and sum leads to same result;

%%
%   Precompute: (A^T*A)^head
a_x=conj(fft_P_x).*fft_P_x;
a_y=conj(fft_P_y).*fft_P_y;
a_xx=conj(fft_P_xx).*fft_P_xx;
a_yy=conj(fft_P_yy).*fft_P_yy;
a_diag=conj(fft_P_diag).*fft_P_diag;

A_t_A_h=w1*a_x+w1*a_y+w2*a_xx+w2*a_yy+w2*a_diag;

%%
%   CG Iteration 


%kernel_pad=padPSF(kernel,[n m]);
%kernel_pad=circshift(kernel_pad,1-center);


fft_kernel=psf2otf(kernel,[n m]); % this does an circshift implicitely

kernel_pad = real(ifft2(fft_kernel)); % same as pad, then circshift

a0 = real(ifft2(A_t_A_h.*fft_kernel)) + beta * kernel_pad;
r = b_bar-a0;

p = r;
i = 0;

while i < MAX_CG_ITER %,norm(r0) > 10000,
    
    % TODO: check if we need psf2otf, or something like fft2
    a = real(ifft2(A_t_A_h.*fft2(p))) + beta * p;
    % b = real(ifft2(A_t_A_h.*psf2otf(p,[n m]))) + beta * p;  
    % TODO: This results
    % in sometehing hugely different. But in line 91 you are using the
    % variant b instead of a for a0. So there has to be something wrong. 
    
    % TODO: r' * r doesnt work as r is a matrix, should be a vector
    s = n * m;
    alpha = (reshape(r, 1, s) * reshape(r, s, 1)) / ...
           (reshape(p, 1, s) * reshape(a,s,1));
       
    % tryed some other norm --> convergence of r much slower
    %alpha=norm(r,'fro')^2/(norm(p,'fro')*norm(a,'fro'));
    
    kernel_pad=kernel_pad+alpha*p;
    r_old = r;
    r = r_old-alpha*a;
    % REMARK: the ':' calculates the Frobenius norm and makes our code about 50s faster and works as well. 
    % The loop is never entered btw. Is this a problem?
    norm_r = norm(r(:)); 
    if  norm_r < CG_STOP_CRIT
        break;
    end
        
    gamma = reshape(r, 1, s) * reshape(r, s, 1) / ...
           (reshape(r_old, 1, s) * reshape(r_old,s,1));
    
    %tryed some other norm --> convergence of r much slower
    %gamma=norm(r,'fro')^2/(norm(r_old,'fro')^2);
    
    p = r+gamma*p;
    i = i + 1;
end
    
%% The kernel has to be adjusted. Values that are 1/20 smaller than the biggest entry are set to zero
% First step detects the maximal entry of the kernel and substract 1/20 of
% this value from our Matrix kernel --> entries which were smaller than this
% value, now are smaller than 0.

% max(A,B) creates a Matrix which takes the bigger values from eather A and
% B. In our case the entries smaller than 0 are set to 0. And the entries
% bigger than 0 are unchanged.

kernel = extract_kernel(kernel_pad, size(kernel, 1));

kernel = max(kernel-(1/20)*max(kernel(:)),0);

% normalize the kernel such that it sums up to 1.
kernel = kernel./(sum(kernel(:)));

end