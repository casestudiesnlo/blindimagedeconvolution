function [ kernel ] = center_kernel( kernel )
%CENTER_KERNEL center a kernel that might be shifted in some way
% assumes kernel is normalized

ks = size(kernel, 1);
kc = (ks-1)/2; % center of kernel
% horizontal
sums = sum(kernel, 1);

running = 0;
i=1;
while running < 0.5
  running = running + sums(i);
  i = i + 1;
end
% now, at i we have more on the left than on the right -> shift by kc-i to
% make i-1 the center

% vertical
sums = sum(kernel, 2);

running = 0;
j=1;
while running < 0.5
  running = running + sums(j);
  j = j + 1;
end


kernel = circshift(kernel, [kc-j+2,kc-i+2]);
end

