function k = sinusKernel(s)
k = zeros(s,s);

for i=1:s
    for j=1:s
        target = sin(i / 2)*2;
        value = j-(s-1)/2;
        
        if(abs(value-target) < 3)
            k(i,j) = 3-abs(value-target);
        end
        
    end
end

% norm and center
k=max(k-(1/20)*max(k(:)),0);
k=k./(sum(k(:)));
k = center_kernel(k);
