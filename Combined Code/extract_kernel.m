function [ small ] = extract_kernel( kernel, size )
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

small = circshift(kernel, [(size-1)/2,(size-1)/2]); 
small = small(1:size, 1:size);
end
