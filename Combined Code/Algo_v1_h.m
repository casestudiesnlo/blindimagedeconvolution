function [deblurred] = Algo_v1_h(PSF, blurred, gamma)

[n,m]=size(blurred);

% transform the kernel, the derivative kernels and the blurred image into
% the fourier domain
fft_b = fft2(blurred,n, m);
fft_psf = psf2otf(PSF,[n m]);
fft_0 = psf2otf(zeros(n,m),[n m]);
fft_d0 = psf2otf(1,[n m]);
fft_dx = psf2otf([-1,0,1],[n m]);
fft_dy = psf2otf([-1;0;1],[n m]);
fft_dxx = psf2otf([-1,0,2,0,-1],[n m]);
fft_dyy = psf2otf([-1;0;2;0;-1],[n m]);
fft_dxy = psf2otf([-1,0,-1;0,0,0;-1,0,-1],[n m]);

% reconstruct the final latent image
delta = 50.*conj(fft_d0).*fft_d0 + 25.*conj(fft_dx).*fft_dx + 25.*conj(fft_dy).*fft_dy + 12.5.*conj(fft_dxx).*fft_dxx +12.5.*conj(fft_dyy).*fft_dyy +12.5.*conj(fft_dxy).*fft_dxy;
deblurred = real(ifft2((conj(fft_psf).*fft_b.*delta + gamma*conj(fft_dx).*fft_0 + gamma*conj(fft_dy).*fft_0)./(conj(fft_psf).*fft_psf.*delta + gamma*conj(fft_dx).*fft_dx + gamma*conj(fft_dy).*fft_dy)));
end