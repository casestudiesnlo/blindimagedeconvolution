Blind Image Deconvolution

============================= / (top level folders)===========================

Combined Code
- Contains all of the code

Test Images
- Contains Images taken at MVTec


============================= / (Combined Code)==============================

----------------------------- / (Combined Code folders)-----------------------

Testscripts
- Contains testscirpts for different purposes (naming should be self explanatory)

Prediction Filter
- Different filter which can be applied at the picture to get improved results in other algorithms

images
- Images used for the GUI

lib
- usefull code mostly used by deblur by Perrone and Favaro

----------------------------- / (Combined Code files)-----------------------

Algo_v1_<>.m
- Different first version of the same Algorithm without PSI and M

Algo_v2.m
- Final version of non-blind deblurring algorithm
- Note for running: Line 12 dependant on Matlab version

Tik.m and Tik2.m 
- Tikhonov regularization Algorithms

deconv_pd_tv.m
- Total variation Algorithm

deblur.m
- Algorithm by Perrone and Favaro

final_presentation
- mini-application shown at the final presentation

coarse_to_fine and deblur_for scale with and without final
- Final version of blind deblurring algorithm (the kernel estimation part, the last non-blind pass has to be done separately, see e.g. testBlindDeblurring.m)
- With final needs an axis so it knows where to show the kernel (only used in final presentation)

gui_test_runner
-GUI to test all algorithms on different images and with different parameters(kernel, noise) uses test_runner_for_gui.m to start algorithms


============================= / (Test Images)==============================

- images taken at MVTec containing only the relevant pictures for each Object